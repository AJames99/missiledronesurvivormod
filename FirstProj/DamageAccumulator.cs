﻿using System;
using RoR2;
using RoR2.Projectile;
using UnityEngine;
using System.Linq;

namespace MissileDroneSurvivor {

	// This component actively monitors the attached HealthComponent for damage, heals it, 
	// and accumulates the damage taken into a value applied to the explosion of the projectile it's attached to.
	class DamageAccumulator : MonoBehaviour {
		//public float maxHP = -1;
		public float accumulatedDamage;
		//public float tunedDamageOutput;
		public float damageMultiplier = 0.175f;//0.15f;//0.1f;//0.05f;//2.5f;
		public float damageToRadiusMultiplier = 0.15f;//1.0f;//0.45f;//1.4f; //1.0f
		private float baseDamageCoeff;
		private float baseRadius;
		private HealthComponent healthComponent;
		private ProjectileImpactExplosion projectileImpactExplosion;

		TeamComponent teamComponent;
		bool forcedTeamYet;
		//TeamFilter teamFilter;

		private void Awake() {
			healthComponent = GetComponent<HealthComponent>();
			projectileImpactExplosion = GetComponent<ProjectileImpactExplosion>();

			// If this hasn't been manually set, just grab it from the healthComponent.
			/*if (maxHP < 0) {
				maxHP = healthComponent.fullHealth;
			}*/

			healthComponent.dontShowHealthbar = true;

			accumulatedDamage = 0;
			baseDamageCoeff = projectileImpactExplosion.blastDamageCoefficient;
			baseRadius = projectileImpactExplosion.blastRadius;

			//teamFilter = GetComponent<TeamFilter>();
			teamComponent = GetComponent<TeamComponent>();
			forcedTeamYet = false;
		}

		public void FixedUpdate() {

			// First calculate the health lost this update cycle
			float healthLost = (healthComponent.fullHealth - healthComponent.health); //maxHP?

			// Then heal back to full HP
			healthComponent.health = healthComponent.fullHealth;
			//healthComponent.health = maxHP;

			// Add the health that was lost this cycle onto the accumulator
			accumulatedDamage += healthLost;

			/*if (healthLost > 0) {
				Chat.AddMessage("fullHP: " + healthComponent.fullHealth + ", healthLost: " + healthLost);
			}*/

			// Adjust the accumulated damage via an S-Curve to prevent linear scaling (It becomes a REAL nuke past lvl. 8 otherwise)
			float tunedAccumulatedDamage = SCurveAccumulatedDamage(accumulatedDamage * damageMultiplier, 50.0f, 0.01f, 45.0f);
			float tunedAccumulatedDamageForRadius = SCurveAccumulatedDamage(accumulatedDamage * damageToRadiusMultiplier, 250.0f, 0.01f, 45.0f);

			projectileImpactExplosion.blastDamageCoefficient = baseDamageCoeff + tunedAccumulatedDamage;
			projectileImpactExplosion.blastRadius = baseRadius + tunedAccumulatedDamageForRadius;

			// Mae hacky iawn ond idgaf.
			if (!forcedTeamYet) {
				teamComponent.teamIndex = TeamIndex.Monster;
				forcedTeamYet = true;
			}

			//Chat.AddMessage("DMG: " + projectileImpactExplosion.blastDamageCoefficient + ", RADIUS: " + projectileImpactExplosion.blastRadius);
		}


		/*const float L = 50f;
		const float k = 0.01f;
		const float s = 45.0f;*/

		// For the min-maxing maths nerds out there, the formula is on Desmos: https://www.desmos.com/calculator/eacdigrslo
		// Given the above values, this gives us the latter half of an s-curve, such that:
		// (if x is damage absorbed, and y is the damage coefficient of the explosion)
		// at x = 50, y = 6.157 (615.7% damage + 500% base coefficient)
		// at x = 100, y = 12.239 (1223.9% damage + 500% base coefficient);
		// at x = 200, y = 21.778 (2177.8% damage coeff. + 500% base coefficient);
		// This begins to really plateau after 300 damage, with 400 damage merely giving an additional 2.23 of extra damage coefficient)
		// This is designed to scale well with insane AoE and attack speeds, but not to the point that the ability alone is going to replace your proc-chain meta.
		// It simply gives this survivor an ability capable of doing some risky but serious AoE in the early-ish game (stages 2-6 or thereabouts)
		// If you've come across this comment, congratulations! Gimme feedback on Discord @HogynMelyn#2589
		private float SCurveAccumulatedDamage(float x, float L, float k, float s) {
			float functionA = L / (1 + Mathf.Exp(-k * (x - s)));
			float offset = L / (1 + Mathf.Exp(k * s));      // This calculates the y offset we need to make the multiplier equal to 0 when x (input damage) is 0.

			return functionA - offset;
		}
	}
}
