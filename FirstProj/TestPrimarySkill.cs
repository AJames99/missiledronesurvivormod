﻿using EntityStates;
using RoR2;
using UnityEngine;

namespace MissileDroneSurvivor.MsIsleEntityStates {
	public class TestPrimarySkill : BaseSkillState {

		public float baseDuration = 0.5f;
		private float duration;
		public GameObject effectPrefab = Resources.Load<GameObject>("prefabs/effects/impacteffects/Hitspark");
		public GameObject hitEffectPrefab = Resources.Load<GameObject>("prefabs/effects/impacteffects/critspark");
		public GameObject tracerEffectPrefab = Resources.Load<GameObject>("prefabs/effects/tracers/tracerbanditshotgun");

		public override void OnEnter() {
			base.OnEnter();
			// Any code can come after base.OnEnter
			//Chat.AddMessage("YOOOO we just entered the Ms. Isle primary skill state!");
			this.duration = this.baseDuration / base.attackSpeedStat;
			Ray aimRay = base.GetAimRay();
			base.StartAimMode(aimRay, 2f, false);
			base.PlayAnimation("Gesture, Override", "FireShotgun", "FireShotgun.playbackRate", this.duration * 1.1f);
			if (base.isAuthority) {
				new BulletAttack {
					owner = base.gameObject,
					weapon = base.gameObject,
					origin = aimRay.origin,
					aimVector = aimRay.direction,
					minSpread = 0f,
					maxSpread = base.characterBody.spreadBloomAngle,
					bulletCount = 1U,
					procCoefficient = 1f,
					damage = base.characterBody.damage,
					force = 3,
					falloffModel = BulletAttack.FalloffModel.DefaultBullet,
					tracerEffectPrefab = this.tracerEffectPrefab,
					hitEffectPrefab = this.hitEffectPrefab,
					isCrit = base.RollCrit(),
					HitEffectNormal = false,
					stopperMask = LayerIndex.world.mask,
					smartCollision = true,
					maxDistance = 300f

				}.Fire();
			}
		}

		public override void OnExit() {
			// Any code can come before base.OnExit
			base.OnExit();
		}

		public override void FixedUpdate() {
			base.FixedUpdate();
			// Any code can come after base.FixedUpdate
			if (base.fixedAge >= this.duration && base.isAuthority) {
				this.outer.SetNextStateToMain();
				return;
			}
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Skill;
		}
	}
}
