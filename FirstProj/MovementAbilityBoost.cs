﻿using System;
using RoR2;
using UnityEngine;
using UnityEngine.Networking;
using R2API.Networking;
using R2API.Networking.Interfaces;
using EntityStates;

namespace MissileDroneSurvivor.MsIsleEntityStates {

	// Token: 0x02000AA2 RID: 2722
	public class DroneBoostAbility : BaseState {

		// Boosts completed since we last were sprinting (i.e. in range of terrain)
		//private int boostsDone = 0;

		// How long does the boost last?
		public float boostDuration = 2.0f;
		private float stopwatch = 0.0f;

		// How much of a multiplier should be applied to the speed stat when boosting? //was 1.55, too low
		public float boostBaseStrength = 2.0f;
		// Per Wax Quail owned, how much should we boost the strength by?
		public float boostStrengthPerUpgrade = 0.15f;
		// How many Wax Quails are in our inv?
		public int quailsOwned = 0;

		private float storedBaseSpeed = 0f;

		//		public static Transform fauxJetLeft;
		//		public static Transform fauxJetRight;

		DronePassiveSkill passiveManager;
		private CharacterBody body;


		// Token: 0x06003DBF RID: 15807 RVA: 0x00101EB8 File Offset: 0x001000B8
		public override void OnEnter() {
			base.OnEnter();

			if (!body) {
				body = gameObject.GetComponent<CharacterBody>();
			}
			if (!passiveManager) {
				passiveManager = gameObject.GetComponent<DronePassiveSkill>();
			}

			//passiveManager.ResetUtilityToDefault();

			// This is (was) to prevent the bandolier bug. (Attempt to anyway)
			/*if (passiveManager.isBoosting) {
				Chat.AddMessage("Cancelled boost; already isBoosting.");
				this.outer.SetNextStateToMain();
				return;
			}*/

			stopwatch = 0;

			passiveManager.OnDroneBoost(this);

			// Set the boost timer
			//boostTimer = boostDuration; //redundant, using EntityState's fixedAge instead

			//Chat.AddMessage("Boosting!");
			Util.PlaySound("Play_drone_repair", base.gameObject);
			//Util.PlaySound("Play_loader_shift_release", base.gameObject);
			Util.PlaySound("Play_loader_m2_launch", base.gameObject);

			// TODO only play this if we have Wax Quails
			/*if (boostEffect) {
				EffectManager.SpawnEffect(boostEffect, new EffectData {
					origin = base.transform.position,
					scale = boostEffectSize
				}, true);
			}*/

			/*if (base.characterMotor && base.characterDirection) {
				base.characterMotor.velocity.y = 0f;
				base.characterMotor.velocity = this.forwardDirection * this.rollSpeed;
			}
			Vector3 b = base.characterMotor ? base.characterMotor.velocity : Vector3.zero;
			this.previousPosition = base.transform.position - b;*/

			// Cache this
			//storedBaseSpeed = body.baseMoveSpeed;
			// Multiply by our effects
			//body.baseMoveSpeed = (body.baseMoveSpeed) * (boostBaseStrength + (boostStrengthPerUpgrade * quailsOwned));
			storedBaseSpeed = base.moveSpeedStat * (boostBaseStrength + (boostStrengthPerUpgrade * quailsOwned));

			Vector3 intendedDirection;
			if (base.rigidbodyMotor.moveVector.magnitude > 0.01f) {
				intendedDirection = base.rigidbodyMotor.moveVector.normalized;
			} else {
				intendedDirection = base.GetAimRay().direction;
			}

			rigidbodyMotor.rigid.AddForce(intendedDirection * storedBaseSpeed, ForceMode.VelocityChange);
		}

		// Token: 0x06003DC1 RID: 15809 RVA: 0x00102144 File Offset: 0x00100344
		public override void FixedUpdate() {
			base.FixedUpdate();
			// FoV Changes
			/*if (base.cameraTargetParams) {
				base.cameraTargetParams.fovOverride = Mathf.Lerp(DodgeState.dodgeFOV, 60f, base.fixedAge / this.duration);
			}*/

			//rigidbodyMotor.moveVector *= storedBaseSpeed;

			stopwatch += Time.fixedDeltaTime; //17102020-1739 added this in, in place of fixedAge since that seems to be utterly useless as a timekeeper. Doesn't need initialisation in the Huntress code? 

			if (stopwatch >= this.boostDuration && base.isAuthority) {
				this.outer.SetNextStateToMain();
				return;     // 17102020-1739 removed this to fix boost bug; basing func off of Huntress' blink now
			}
		}

		// Fuck you. Only death may interrupt this skill. I am fucking done with skill interrupts. DIE.
		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Death;
		}

		// Token: 0x06003DC2 RID: 15810 RVA: 0x00102275 File Offset: 0x00100475
		public override void OnExit() {
			/* 
			// Undo the FoV Override
			if (base.cameraTargetParams) {
				base.cameraTargetParams.fovOverride = -1f;
			}
			*/

			// Undo sprint speed boost
			//body.baseMoveSpeed = storedBaseSpeed;

			passiveManager.OnDroneBoostExit(this);

			//Chat.AddMessage("Boost - OnExit. Stopwatch: " + stopwatch);

			base.OnExit();
		}

		// Token: 0x06003DC3 RID: 15811 RVA: 0x0010229A File Offset: 0x0010049A
		/*public override void OnSerialize(NetworkWriter writer) {
			base.OnSerialize(writer);
			writer.Write(this.forwardDirection);
		}

		// Token: 0x06003DC4 RID: 15812 RVA: 0x001022AF File Offset: 0x001004AF
		public override void OnDeserialize(NetworkReader reader) {
			base.OnDeserialize(reader);
			this.forwardDirection = reader.ReadVector3();
		}*/

		// Token: 0x04003908 RID: 14600
		//[SerializeField]
		//public float duration = 0.9f;

		// Token: 0x04003909 RID: 14601
		//[SerializeField]
		//public float initialSpeedCoefficient;

		// Token: 0x0400390A RID: 14602
		//[SerializeField]
		//public float finalSpeedCoefficient;

		// Token: 0x0400390B RID: 14603
		//public static string dodgeSoundString;

		// Token: 0x0400390D RID: 14605
		public static float dodgeFOV;

		// Token: 0x0400390E RID: 14606
		//private float rollSpeed;

		// Token: 0x0400390F RID: 14607
		//private Vector3 forwardDirection;

		// Token: 0x04003910 RID: 14608
		//private Animator animator;

		// Token: 0x04003911 RID: 14609
		//private Vector3 previousPosition;
	}
}
