﻿using System;
using RoR2;
using RoR2.Projectile;
using EntityStates;
using UnityEngine;

namespace MissileDroneSurvivor.MsIsleEntityStates {
	public class NukeAbility : BaseState {

		//const int numRocketsBase = 4;
		//const float rocketFireIntervalBase = 0.5f;
		//const float missileRecharge = 2.0f; // in seconds

		// Token: 0x06003D45 RID: 15685 RVA: 0x000FFE34 File Offset: 0x000FE034
		private void FireMissile(string targetMuzzle) {
			base.PlayAnimation("Gesture, Additive", "FireMissile");

			Ray aimRay = base.GetAimRay();

			if (this.modelTransform) {
				ChildLocator component = this.modelTransform.GetComponent<ChildLocator>();
				if (component) {
					Transform transform = component.FindChild(targetMuzzle);
					if (transform) {
						aimRay.origin = transform.position;
					}
				}
			}
			if (effectPrefab) {
				EffectManager.SimpleMuzzleFlash(effectPrefab, base.gameObject, targetMuzzle, false);
			}
			if (base.characterBody) {
				base.characterBody.SetAimTimer(2f);
			}

			if (base.isAuthority) {
				float x = UnityEngine.Random.Range(minSpread, maxSpread);
				float z = UnityEngine.Random.Range(0f, 360f);
				Vector3 up = Vector3.up;
				Vector3 axis = Vector3.Cross(up, aimRay.direction);
				Vector3 vector = Quaternion.Euler(0f, 0f, z) * (Quaternion.Euler(x, 0f, 0f) * Vector3.forward);
				float y = vector.y;
				vector.y = 0f;
				float angle = Mathf.Atan2(vector.z, vector.x) * 57.29578f - 90f;
				float angle2 = Mathf.Atan2(y, vector.magnitude) * 57.29578f;
				Vector3 forward = Quaternion.AngleAxis(angle, up) * (Quaternion.AngleAxis(angle2, axis) * aimRay.direction);

				ProjectileManager.instance.FireProjectile(projectilePrefabNuke, aimRay.origin, Util.QuaternionSafeLookRotation(forward), base.gameObject, this.damageStat * damageCoefficient, 100f, Util.CheckRoll(this.critStat, base.characterBody.master), DamageColorIndex.Default, null, -1f);
				Util.PlaySound("Play_engi_seekerMissile_shoot", base.gameObject);
				hasFired = true;
			}
		}

		// Token: 0x06003D46 RID: 15686 RVA: 0x00100012 File Offset: 0x000FE212
		public override void OnEnter() {
			base.OnEnter();

			this.modelTransform = base.GetModelTransform();
			//this.fireInterval = baseFireInterval / this.attackSpeedStat;

			hasFired = false;

			this.fireTimer = baseFireDelay;

			Util.PlaySound("Play_engi_seekerMissile_lockOn", base.gameObject);
			Util.PlaySound("Play_engi_seekerMissile_HUD_open", base.gameObject);
			/*
			*Play_engi_seekerMissile_lockOn
			* Play_engi_seekerMissile_shoot
			* Play_engi_seekerMissile_HUD_open
			* */
		}

		// Token: 0x06003D47 RID: 15687 RVA: 0x00032EEB File Offset: 0x000310EB
		public override void OnExit() {
			base.OnExit();
		}

		// Token: 0x06003D48 RID: 15688 RVA: 0x00100038 File Offset: 0x000FE238
		public override void FixedUpdate() {
			base.FixedUpdate();

			this.fireTimer -= Time.fixedDeltaTime;

			if (!hasFired) {
				if (this.fireTimer <= 0f) {
					this.FireMissile("Muzzle");

					if (base.isAuthority) {
						this.outer.SetNextStateToMain();
						return;
					}
				}
			}
		}

		// Token: 0x06003D49 RID: 15689 RVA: 0x0000D2C7 File Offset: 0x0000B4C7
		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Skill;
		}

		// Token: 0x0400386E RID: 14446
		public static GameObject effectPrefab;

		// Token: 0x0400386F RID: 14447
		public static GameObject projectilePrefabNuke;

		// Token: 0x04003870 RID: 14448
		public static float damageCoefficient = 5f;

		// Token: 0x04003871 RID: 14449
		public static float baseFireDelay = 0.2f;//0.75f;

		private bool hasFired;

		private float fireTimer;

		// Token: 0x04003872 RID: 14450
		public static float minSpread = 0f;

		// Token: 0x04003873 RID: 14451
		public static float maxSpread = 5f;

		// Token: 0x04003877 RID: 14455
		private Transform modelTransform;

		// Token: 0x04003878 RID: 14456
		private AimAnimator aimAnimator;
	}
}
