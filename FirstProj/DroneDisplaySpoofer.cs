﻿using System;
using RoR2;
using RoR2.Projectile;
using UnityEngine;
using System.Linq;
using System.Collections;

namespace MissileDroneSurvivor {
	class DroneDisplaySpoofer : MonoBehaviour {
		// This wouldn't have to be a new component if I just properly edited the animation + animation events to accomplish this, but i cba with that.
		// Just plays sounds and particle effects when appropriate

		class soundEffect {
			public soundEffect(string name, float timeToPlay) {
				this.soundName = name;
				this.timeToPlay = timeToPlay;
				playbackID = 0;
				hasID = false;
			}
			public string soundName;
			public float timeToPlay;
			public uint playbackID;
			public bool hasID;
		}

		class vfxEffect {
			public vfxEffect(GameObject effect, float timeToPlay) {
				this.effect = effect;
				this.timeToPlay = timeToPlay;
			}
			public GameObject effect;
			public float timeToPlay;
		}

		vfxEffect[] effects;
		soundEffect[] soundEffects;
		Vector3 targetDronePosition;
		Vector3 currentDronePosition;
		Vector3 hoverCycleOffset;
		//const float hoverCycleDistanceY = 0.12f; // might need to decrease this some more?
		const float hoverCycleDistanceY = 0.08f;
		//const float hoverCycleDistanceXZ = 0.01f;
		const float hoverCycleDistanceXZ = 0.04f;

		void Awake() {

			effects = new vfxEffect[] {
				new vfxEffect(Resources.Load<GameObject>("prefabs/effects/impacteffects/CharacterLandImpact"), 0.2f),
				new vfxEffect(Resources.Load<GameObject>("Prefabs/effects/impacteffects/BootShockwave"), 0.35f)
			};

			soundEffects = new soundEffect[] {
				new soundEffect("Play_drone_repair", 0.35f),
				new soundEffect("Play_drone_active_loop", 0.35f),
				new soundEffect("Play_engi_R_turret_spawn", 0.1f)
			};

			foreach (vfxEffect effect in effects) {
				StartCoroutine(DelayEffect(effect));
			}

			foreach (soundEffect effect in soundEffects) {
				StartCoroutine(DelaySound(effect));
			}


			// This component is attached to mdlMissileDrone
			// Destroy the stock animations that stop us from manually moving the prefab about
			AimAnimator aimAnimator = GetComponent<AimAnimator>();
			Destroy(aimAnimator);
			Animator displayAnimator = GetComponent<Animator>();
			Destroy(displayAnimator);

			//displayArmature.position += new Vector3(0.0f, 2.3f, 0.0f);
			//transform.position += new Vector3(0.0f, 1.5f, 0.0f);

			//transform.position += new Vector3(2.0f, 4.0f, 0.0f);
			targetDronePosition = transform.position + new Vector3(0.0f, 1.25f, 0.0f);
			currentDronePosition = transform.position + new Vector3(3.0f, 5.0f, 0.0f);
		}

		void Update() {
			// time is in secs, sin works in degrees
			hoverCycleOffset = new Vector3(Mathf.Sin(Time.time) * hoverCycleDistanceXZ, Mathf.Sin(Time.time * 2) * hoverCycleDistanceY, Mathf.Sin(Time.time * 0.75f) * hoverCycleDistanceXZ);

			if ((targetDronePosition - currentDronePosition).magnitude > 0.05f) {
				currentDronePosition = Vector3.Lerp(currentDronePosition, targetDronePosition, 0.985f * Time.deltaTime);
			}

			transform.position = currentDronePosition + hoverCycleOffset;
		}


		IEnumerator DelayEffect(vfxEffect effect) {
			yield return new WaitForSeconds(effect.timeToPlay);
			EffectManager.SpawnEffect(effect.effect, new EffectData {
				origin = base.transform.position,
				scale = 1.0f
			}, true);
		}

		IEnumerator DelaySound(soundEffect effect) {
			yield return new WaitForSeconds(effect.timeToPlay);
			effect.playbackID = Util.PlaySound(effect.soundName, base.gameObject);
			effect.hasID = true;
		}

		void OnDestroy() {
			//Debug.Log("    >|DRONE DISPLAY DESTROYED! (Good!)");
			foreach (soundEffect effect in soundEffects) {
				//if (effect.hasID) {
				AkSoundEngine.StopPlayingID(effect.playbackID);
				//Debug.Log("        >| Killing sound with playbackID " + effect.playbackID);
				//}
			}
		}
	}
}
