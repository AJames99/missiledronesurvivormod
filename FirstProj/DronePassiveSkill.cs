﻿using System;
using RoR2;
using RoR2.Projectile;
using UnityEngine;
using System.Linq;


namespace MissileDroneSurvivor {
	public class DronePassiveSkill : MonoBehaviour {
		private CharacterBody body;
		private KillCounterComponent killCounter;
		private InputBankTest inputBank;
		private RigidbodyMotor motor;

		// The radius to check within to initialise the sprint
		public float radiusToCheckEntrance = 5.0f; //4

		// Once sprinting, this radius is used to check if we should end the sprint instead. This should be the same as or larger than the entrance radius.
		public float radiusToCheckExit = 8.0f; //6

		// Boost ability
		public GenericSkill utilitySkill;
		public RoR2.Skills.SkillDef utilitySkillDef;
		private int boostsDone = 0;
		// The base number of maximum jumps
		//		private int maximumBoosts = 0;
		private bool _isBoosting = false;
		public bool isBoosting {
			private set {
				_isBoosting = value;
			}
			get {
				return _isBoosting;
			}
		}
		private Transform jetEffectLeft;
		private Transform jetEffectRight;
		GameObject jetEffect;

		// The scale of the boost effect
		float boostEffectSize = 1.0f;
		// The fx to create when boosting (null = none)
		public static string boostEffectString = "prefabs/effects/impacteffects/CharacterLandImpact";
		static GameObject boostEffect = null;

		// TODO make maixmumBoosts update from the player's multi-jump stat/buff
		// TODO make wax quails cause a larger vertical boost when the player is currently in the sprinting state


		private LayerMask layerMask;
		//private Collider[] results;
		bool isSprinting = false;

		private float verticalMoveSpeed = 10.0f;

		private bool _isNukeInWorld;
		public bool isNukeInWorld {
			private set {
				_isNukeInWorld = value;
			}
			get {
				return _isNukeInWorld;
			}
		}


		// Managing the manner in which the secondary ability functions
		// The actual stocks displayed on the UI and held by the skilldef/skill are purely cosmetic and indicative
		// On use, all stocks are set to 0, the max is set to 0, and whatever the value was is passed BY THIS component to the pack hound state machine
		//public RoR2.Skills.SkillDef packHoundsSkillDef = null;
		public GenericSkill packHoundsSkill = null;
		//		private int killCountLastUse = 0;   // How many kills did we have last time we activated the packhounds?
		//		private int lastRecordedKillCount = 0;
		private int _packHoundCount = 0;
		public int packHoundCount {
			get { return _packHoundCount; }
			set { _packHoundCount = value; }
		}

		public void Awake() {
			body = GetComponent<CharacterBody>();

			inputBank = body.GetComponent<InputBankTest>();
			motor = body.GetComponent<RigidbodyMotor>();
			killCounter = body.GetComponent<KillCounterComponent>();
			if (!killCounter) {
				killCounter = body.gameObject.AddComponent<KillCounterComponent>();
			}

			// world is index 11 in the layers?
			layerMask = 1 << LayerMask.NameToLayer("World");
			isSprinting = false;
			//boostsDone = 0;

			// Add the jet boost effect transforms (Boy do I love editing prefabs purely via code lol)
			GameObject empty = new GameObject("empty");
			Transform bodyJetAttachment = body.GetComponent<ModelLocator>().modelBaseTransform.Find("mdlMissileDrone").Find("MissileDroneArmature").Find("ROOT").Find("Body");
			//Transform bodyJetAttachment = bodyComponent.transform; //.GetComponent<ModelLocator>().modelBaseTransform.Find("mdlMissileDrone");

			if (jetEffect == null) {
				jetEffect = EntityStates.Commando.DodgeState.jetEffect;
				//Chat.AddMessage(jetEffect == null ? "    >|FAILED to get Commando's jet effect" : "    >|Got commando's jet effect - " + jetEffect.name);
			}

			boostEffect = Resources.Load<GameObject>(boostEffectString);

			jetEffectLeft = Instantiate(empty, bodyJetAttachment).transform;
			jetEffectLeft.SetParent(bodyJetAttachment);
			jetEffectLeft.transform.localPosition = new Vector3(-0.61f, 0.0f, -0.769f);
			jetEffectLeft.name = "jetEffectLeft";
			//Vector3 jetEffectLeftLookat = (jetEffectLeft.position - bodyJetAttachment.position) * 3.0f;
			jetEffectLeft.LookAt(bodyJetAttachment.position + (-3 * bodyJetAttachment.forward) + (-0.9f * bodyJetAttachment.right), Vector3.up);

			jetEffectRight = Instantiate(empty, bodyJetAttachment).transform;
			jetEffectRight.SetParent(bodyJetAttachment);
			jetEffectRight.transform.localPosition = new Vector3(0.61f, 0.0f, -0.769f);
			jetEffectRight.name = "jetEffectRight";
			//Vector3 jetEffectRightLookat = (jetEffectRight.position - bodyJetAttachment.position) * 3.0f;
			jetEffectRight.LookAt(bodyJetAttachment.position + (-3 * bodyJetAttachment.forward) + (0.9f * bodyJetAttachment.right), Vector3.up);

			if (body.inventory) {
				//maximumBoosts = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
				utilitySkillDef.baseMaxStock = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
				//Chat.AddMessage("utilitySkillDef.baseMaxStock set to " + utilitySkillDef.baseMaxStock + "(" + body.baseJumpCount + " + " + body.inventory.GetItemCount(ItemIndex.Feather) + " + " + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine) + ")");

				//quailsOwned = body.inventory.GetItemCount(ItemIndex.JumpBoost);
			} else {
				utilitySkillDef.baseMaxStock = body.baseJumpCount;
				//Chat.AddMessage("utilitySkillDef.baseMaxStock set to " + utilitySkillDef.baseMaxStock + ", maxJumps = " + body.baseJumpCount);
				//quailsOwned = 0;
			}

			utilitySkill.stock = utilitySkillDef.baseMaxStock;
			//Chat.AddMessage("utilitySkill.stock set to " + utilitySkill.stock);

			packHoundsSkill.stock = 0;
		}

		public void FixedUpdate() {

			/*if (packHoundsSkillDef) {
				if (lastRecordedKillCount != body.killCountServer) {
					Chat.AddMessage("Killcount updated for " + body.GetUserName() + ", id: " + body.networkIdentity.netId + " - " + body.killCountServer + "kills.");

					packHoundCount += (body.killCountServer - lastRecordedKillCount) * (1 + body.inventory.GetItemCount(ItemIndex.SecondarySkillMagazine));
					packHoundsSkillDef.baseMaxStock = packHoundCount;
					packHoundsSkill.stock = packHoundCount;
					packHoundsSkillDef.stockToConsume = packHoundCount;
					packHoundsSkillDef.requiredStock = Mathf.Max(packHoundCount - 1, 1); //liable to be the source of the bug?
					lastRecordedKillCount = body.killCountServer;

					//packHoundsSkillDef.baseMaxStock += (body.killCountServer - lastRecordedKillCount) * (1 + body.inventory.GetItemCount(ItemIndex.SecondarySkillMagazine));
					//lastRecordedKillCount = body.killCountServer;
					//packHoundsSkillDef.requiredStock = packHoundsSkillDef.baseMaxStock;
					//packHoundsSkill.stock = packHoundsSkillDef.baseMaxStock;
					//packHoundsSkillDef.stockToConsume = packHoundsSkillDef.baseMaxStock;
					//packHoundCount = packHoundsSkillDef.baseMaxStock;
				}
		}*/

			// Don't run any of this if the Drone is using its utility ability
			if (!isBoosting) {
				//Otherwise check the sprint based on fly-by radius as usual
				if (Physics.CheckSphere(body.corePosition, isSprinting ? radiusToCheckExit : radiusToCheckEntrance, layerMask, QueryTriggerInteraction.Ignore)) {
					// Start sprinting AND reset all our jumps & double jumps etc
					if (!isSprinting) {
						isSprinting = true;
						body.isSprinting = isSprinting;


						// Need to refresh this frequently to stop hardlight afterburners being inconsistent
						utilitySkillDef.baseMaxStock = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);

						if (utilitySkill.stock < utilitySkillDef.baseMaxStock) {
							//boostsDone = 0;
							utilitySkill.stock = utilitySkillDef.baseMaxStock;
							//Chat.AddMessage("Restoring utility stock");
						} else {
							//Chat.AddMessage("Stock was already full!");
						}
					}
				} else if (isSprinting) {
					isSprinting = false;
					body.isSprinting = isSprinting;
				}

				// This was used to add 1 stock on every time we finished boosting for every extra double jump we had. 
				// Now redundant due to using proper stock system
				/* else if (boostsDone < maximumBoosts) {
					utilitySkill.stock = 1;
				}*/
			}

			/*if (inputBank.jump.justPressed) {
				InputBoost();
			}*/
			if (inputBank.jump.down) {
				motor.moveVector += new Vector3(0.0f, verticalMoveSpeed, 0.0f);
			}

			// Are we physically pressing the sprint key? (Need this workaround otherwise being in the sprinting state means inputBank thinks ctrl is being pressed)
			// (This respects alternative keybindings though)
			if (LocalUserManager.GetFirstLocalUser().inputPlayer.GetButton("sprint")) {
				motor.moveVector -= new Vector3(0.0f, verticalMoveSpeed, 0.0f);
			}

			/* else {
				// Gradually move us down if we're neither boosting nor sprinting (kinda annoying; removed)
				if (!isSprinting) {
					motor.moveVector += new Vector3(0.0f, -2.5f, 0.0f);
				}
			}*/

			// Clamp our vertical component for the moveVector so press W/S can't increase the vertical speed beyond our set vertical movement speed
			motor.moveVector.y = Mathf.Clamp(motor.moveVector.y, -verticalMoveSpeed, verticalMoveSpeed);

			// I hate to do this every frame, but hardlight afterburner won't shut the fuck up
			if (utilitySkill.stock > utilitySkillDef.baseMaxStock) {
				utilitySkill.stock = utilitySkillDef.baseMaxStock;
			}

			if (packHoundsSkill.stock != packHoundCount) {
				packHoundsSkill.stock = packHoundCount;
			}
		}

		// This replaces the shitty old system for monitoring kills to fund Pack Hounds
		public void AddKill() {
			//packHoundsSkillDef.baseMaxStock = packHoundCount;
			//packHoundsSkillDef.stockToConsume = packHoundCount;
			//packHoundsSkillDef.requiredStock = Mathf.Max(packHoundCount - 1, 1); //liable to be the source of the bug?

			// UPDATE removed changes to SkillDef since they break multiplayer (multiple chars sharing the same skilldef and editing its stats? nah.)
			packHoundCount += 1 + body.inventory.GetItemCount(ItemIndex.SecondarySkillMagazine);
			packHoundsSkill.stock = packHoundCount;
		}

		public void OnPackHoundsUsed() {
			//killCountLastUse = body.killCountServer;
			// REMOVED due to multiplayer bug
			//packHoundsSkillDef.baseMaxStock = 0;

			packHoundsSkill.stock = 0;
			packHoundCount = 0;
		}

		// To avoid Hardlight Afterburner from breaking this ability (Allowing it to have >1 stock at once), reset its definition on each use.
		/*public void ResetUtilityToDefault() {
			utilitySkillDef.baseMaxStock = 1;
			utilitySkillDef.baseRechargeInterval = 0.0f;
			utilitySkillDef.rechargeStock = 0;
			utilitySkillDef.requiredStock = 1;
			utilitySkillDef.shootDelay = 8.0f;  // probably does nothing but w/e
			utilitySkillDef.stockToConsume = 1;
			Chat.AddMessage("Reset Utility ability to default params.");
		}*/

		public void OnDroneBoost(MsIsleEntityStates.DroneBoostAbility boostAbility) {
			// First, check to see if the player's character has multiple jumps (Hopoo Feather) and Hardlight Afterburners
			//maximumBoosts = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
			utilitySkillDef.baseMaxStock = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
			//Chat.AddMessage("utilitySkillDef.baseMaxStock set to " + utilitySkillDef.baseMaxStock);

			// Secondly, check to see if the player owns any jump-boosters (Wax Quails)
			boostAbility.quailsOwned = body.inventory.GetItemCount(ItemIndex.JumpBoost);

			// Force the skillDef to reflect what we want need it to in order to avoid Hardlight Afterburner from causing the infinite boost bug.
			//ResetUtilityToDefault();

			// The effects
			if (jetEffect) {
				if (jetEffectLeft) {
					UnityEngine.Object.Instantiate<GameObject>(jetEffect, jetEffectLeft);
				}
				if (jetEffectRight) {
					UnityEngine.Object.Instantiate<GameObject>(jetEffect, jetEffectRight);
				}
			}
			EffectManager.SpawnEffect(boostEffect, new EffectData {
				origin = body.transform.position,
				scale = boostEffectSize
			}, true);

			// Leads to the infinite boost bug... dont do this
			//utilitySkillDef.baseMaxStock = maximumBoosts;

			isBoosting = true;

			//boostsDone++;
		}

		public void OnDroneBoostExit(MsIsleEntityStates.DroneBoostAbility boostAbility) {
			// Check these again (might as well)
			//maximumBoosts = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
			boostAbility.quailsOwned = body.inventory.GetItemCount(ItemIndex.JumpBoost);
			//utilitySkillDef.baseMaxStock = maximumBoosts;


			utilitySkillDef.baseMaxStock = body.baseJumpCount + body.inventory.GetItemCount(ItemIndex.Feather) + body.inventory.GetItemCount(ItemIndex.UtilitySkillMagazine);
			//Chat.AddMessage("utilitySkillDef.baseMaxStock set to " + utilitySkillDef.baseMaxStock);

			// If we never left the sprint radius, refill automatically.
			if (isSprinting) {
				utilitySkill.stock = utilitySkill.maxStock;
			}


			// The current time should be >= the time we marked when we entered the boost state.
			/*float expectedVsReality = Time.time - boostTimerPassive;
			// Did the boost exit prematurely? If so, cancel the rest of this function. (<0 ideally, but -0.01 to give a tiny millisecond's margin of error)
			if (expectedVsReality < -0.01f) {
				Chat.AddMessage("Early exit of OnDroneBoostExit; Time difference was " + expectedVsReality);
				return;
			}*/

			isBoosting = false;
		}
	}
}
