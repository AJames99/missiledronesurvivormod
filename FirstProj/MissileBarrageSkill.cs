﻿using System;
using RoR2;
using RoR2.Projectile;
using EntityStates;
using UnityEngine;


namespace MissileDroneSurvivor.MsIsleEntityStates {
	// Token: 0x02000A8D RID: 2701
	public class MissileBarrage : BaseState {

		//const int numRocketsBase = 4;
		const int numRocketsBase = 6;
		const float rocketFireIntervalBase = 0.5f;
		const float missileRecharge = 2.0f; // in seconds

		// Token: 0x06003D45 RID: 15685 RVA: 0x000FFE34 File Offset: 0x000FE034
		private void FireMissile(string targetMuzzle) {
			this.missileCount++;
			base.PlayAnimation("Gesture, Additive", "FireMissile");
			Ray aimRay = base.GetAimRay();
			if (this.modelTransform) {
				ChildLocator component = this.modelTransform.GetComponent<ChildLocator>();
				if (component) {
					Transform transform = component.FindChild(targetMuzzle);
					if (transform) {
						aimRay.origin = transform.position;
					}
				}
			}
			if (effectPrefab) {
				EffectManager.SimpleMuzzleFlash(effectPrefab, base.gameObject, targetMuzzle, false);
			}

			// What is this for?
			if (base.characterBody) {
				base.characterBody.SetAimTimer(2f);
			}


			if (base.isAuthority) {
				float x = UnityEngine.Random.Range(minSpread, maxSpread);
				float z = UnityEngine.Random.Range(0f, 360f);
				Vector3 up = Vector3.up;
				Vector3 axis = Vector3.Cross(up, aimRay.direction);
				Vector3 vector = Quaternion.Euler(0f, 0f, z) * (Quaternion.Euler(x, 0f, 0f) * Vector3.forward);
				float y = vector.y;
				vector.y = 0f;
				float angle = Mathf.Atan2(vector.z, vector.x) * 57.29578f - 90f;
				float angle2 = Mathf.Atan2(y, vector.magnitude) * 57.29578f;
				Vector3 forward = Quaternion.AngleAxis(angle, up) * (Quaternion.AngleAxis(angle2, axis) * aimRay.direction);

				ProjectileManager.instance.FireProjectile(projectilePrefab, aimRay.origin, Util.QuaternionSafeLookRotation(forward), base.gameObject, this.damageStat * damageCoefficient, 0f, Util.CheckRoll(this.critStat, base.characterBody.master), DamageColorIndex.Default, null, -1f);
			}
		}

		// Token: 0x06003D46 RID: 15686 RVA: 0x00100012 File Offset: 0x000FE212
		public override void OnEnter() {
			base.OnEnter();

			this.modelTransform = base.GetModelTransform();
			//this.fireInterval = baseFireInterval / this.attackSpeedStat;

			// New stuff!!
			float attackSpeed = this.attackSpeedStat;
			float degreeToWhichAttackSpeedHasBeenIncreasedOverall = attackSpeed / baseFireInterval;
			//Chat.AddMessage("this.attackSpeedStat = " + this.attackSpeedStat + ", baseFireInterval = " + baseFireInterval);

			// Base(4) + (Floor of Level x 1.5) + (1 for every extra 15% of increased firerate we have)
			// This first version scaled WAAAAY too quickly with level. Moving to a version where team Level has much reduced effect.
			//int numRockets = numRocketsBase + Mathf.FloorToInt(1.5f * TeamManager.instance.GetTeamLevel(this.GetTeam()) - 1) + Mathf.FloorToInt((degreeToWhichAttackSpeedHasBeenIncreasedOverall - 1) / 0.15f);
			// 4 + (1 every 4 levels) + (1 per 30% increase to firerate)
			//int extraRocketsForLevel = Mathf.RoundToInt(0.25f * TeamManager.instance.GetTeamLevel(this.GetTeam()) - 1);
			int extraRocketsForFirerate = Mathf.RoundToInt((degreeToWhichAttackSpeedHasBeenIncreasedOverall - 1) / 0.30f);
			int numRockets = numRocketsBase + /*extraRocketsForLevel +*/ extraRocketsForFirerate;

			// Fire interval is such that the full burst is completed over 2 seconds, always. Increased firerate just increases the # of missiles
			//float newinterval = rocketFireIntervalBase * ((float)numRocketsBase / (float)numRockets);
			float newinterval = missileRecharge / (2 * numRockets); // better means of calculation
			fireInterval = newinterval;

			maxMissileCount = numRockets;

			//Chat.AddMessage("maxMissileCount: " + numRockets + "(calculated from: 4 + 0.25x" + TeamManager.instance.GetTeamLevel(this.GetTeam() - 1) + " + (" + (degreeToWhichAttackSpeedHasBeenIncreasedOverall - 1) + ") / 0.3" + ", fireInterval: " + fireInterval);
			//Chat.AddMessage("maxMissileCount:" + numRocketsBase + " + " + /*extraRocketsForLevel + " + " +*/ extraRocketsForFirerate + " = " + numRockets);

			EntityStates.Drone.DroneWeapon.FireMissileBarrage.minSpread = 0.0f;
			//EntityStates.Drone.DroneWeapon.FireMissileBarrage.maxSpread = 7.5f;
			EntityStates.Drone.DroneWeapon.FireMissileBarrage.maxSpread = 3.5f;
		}

		// Token: 0x06003D47 RID: 15687 RVA: 0x00032EEB File Offset: 0x000310EB
		public override void OnExit() {
			base.OnExit();
		}

		// Token: 0x06003D48 RID: 15688 RVA: 0x00100038 File Offset: 0x000FE238
		public override void FixedUpdate() {
			base.FixedUpdate();
			this.fireTimer -= Time.fixedDeltaTime;
			if (this.fireTimer <= 0f) {
				this.FireMissile("Muzzle");
				this.fireTimer += this.fireInterval;
			}
			if (this.missileCount >= maxMissileCount && base.isAuthority) {
				this.outer.SetNextStateToMain();
				return;
			}
		}

		// Token: 0x06003D49 RID: 15689 RVA: 0x0000D2C7 File Offset: 0x0000B4C7
		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Skill;
		}

		// Token: 0x0400386E RID: 14446
		public static GameObject effectPrefab;

		// Token: 0x0400386F RID: 14447
		public static GameObject projectilePrefab;
		//public GameObject projectilePrefab; // removed static keyword, may have been preventing the SetProperty

		// Token: 0x04003870 RID: 14448
		public static float damageCoefficient = 1f;

		// Token: 0x04003871 RID: 14449
		public static float baseFireInterval = 1.0f;

		// Token: 0x04003872 RID: 14450
		public static float minSpread = 0f;

		// Token: 0x04003873 RID: 14451
		public static float maxSpread = 5f;

		// Token: 0x04003874 RID: 14452
		public static int maxMissileCount;

		// Token: 0x04003875 RID: 14453
		private float fireTimer;

		// Token: 0x04003876 RID: 14454
		private float fireInterval;

		// Token: 0x04003877 RID: 14455
		private Transform modelTransform;

		// Token: 0x04003878 RID: 14456
		private AimAnimator aimAnimator;

		// Token: 0x04003879 RID: 14457
		private int missileCount;
	}
}
