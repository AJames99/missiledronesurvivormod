﻿using System;
using UnityEngine;
using R2API;
using RoR2;
using R2API.Utils;

namespace MissileDroneSurvivor {
	public static class Skins {
		public static void RegisterSkins() {
			GameObject bodyPrefab = MissileDroneMod.msIsleCharacter;

			GameObject model = bodyPrefab.GetComponentInChildren<ModelLocator>().modelTransform.gameObject;
			CharacterModel characterModel = model.GetComponent<CharacterModel>();

			if (model.GetComponent<ModelSkinController>()) MissileDroneMod.Destroy(model.GetComponent<ModelSkinController>());

			ModelSkinController skinController = model.AddComponent<ModelSkinController>();
			ChildLocator childLocator = model.GetComponent<ChildLocator>();

			SkinnedMeshRenderer mainRenderer = Reflection.GetFieldValue<SkinnedMeshRenderer>(characterModel, "mainSkinnedMeshRenderer");

			LanguageAPI.Add("MSISLE_DEFAULT_SKIN_NAME", "Stock");

			LoadoutAPI.SkinDefInfo skinDefInfo = default(LoadoutAPI.SkinDefInfo);
			skinDefInfo.BaseSkins = Array.Empty<SkinDef>();

			skinDefInfo.GameObjectActivations = Array.Empty<SkinDef.GameObjectActivation>();

			skinDefInfo.Icon = LoadoutAPI.CreateSkinIcon(new Color(68f / 255f, 146f / 255f, 104 / 255f), new Color(0f, 46f / 255f, 93f / 255f), new Color(1f, 1f, 239f / 255f), new Color(0f, 8f / 255f, 16f / 255f));
			skinDefInfo.MeshReplacements = new SkinDef.MeshReplacement[0];
			/*{
				new SkinDef.MeshReplacement
				{
					renderer = mainRenderer,
					mesh = mainRenderer.sharedMesh
				}
			};*/
			skinDefInfo.Name = "MSISLE_DEFAULT_SKIN_NAME";
			skinDefInfo.NameToken = "MSISLE_DEFAULT_SKIN_NAME";
			skinDefInfo.RendererInfos = characterModel.baseRendererInfos;
			skinDefInfo.RootObject = model;
			skinDefInfo.UnlockableName = "";
			skinDefInfo.MinionSkinReplacements = new SkinDef.MinionSkinReplacement[0];
			skinDefInfo.ProjectileGhostReplacements = new SkinDef.ProjectileGhostReplacement[0];

			SkinDef defaultSkin = LoadoutAPI.CreateNewSkinDef(skinDefInfo);

			skinController.skins = new SkinDef[1]
			{
				defaultSkin
			};
		}
	}
}