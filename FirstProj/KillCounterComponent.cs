﻿using System;
using RoR2;
using RoR2.Projectile;
using UnityEngine;
using UnityEngine.Networking;
using R2API.Utils;
using R2API;
using R2API.Networking;
using R2API.Networking.Interfaces;
using System.Linq;

namespace MissileDroneSurvivor {
	// A more generic function
	public class MasterKillCounter : MonoBehaviour {

		private int _killCount;
		public int killCount {
			get {
				return _killCount;
			}
			private set {
				_killCount = value;
			}
		}

		public void AddKill(DronePassiveSkill passiveSkill) {
			killCount++;
			//if (!passiveSkill) {
			//	passiveSkill = GetComponentInChildren<DronePassiveSkill>();
			//}
			if (passiveSkill) {
				passiveSkill.AddKill();
			}
		}
	}

	/*public class KillCounterCommand : R2API.Networking.Interfaces.INetCommand {
		public void OnReceived() {

		}
	}*/

	public class SyncSomething : INetMessage {
		NetworkInstanceId netID;
		Vector3 position;
		int number;

		public SyncSomething() {

		}

		public SyncSomething(NetworkInstanceId netID, Vector3 position, int num) {
			this.netID = netID;
			this.position = position;
			number = num;
		}

		public void Deserialize(NetworkReader reader) {
			netID = reader.ReadNetworkId();
			position = reader.ReadVector3();
			number = reader.ReadInt32();
		}

		public void OnReceived() {
			// Are we the host? (Are we running the Network Server)
			if (NetworkServer.active) {
				Debug.Log("SyncSomething: Host ran the OnReceived; skipping.");
				return;
			}
			//Chat.AddMessage($"Client received SyncSomething. Position receiveed is {position}. Number received is {number}.");
			GameObject bodyObject = Util.FindNetworkObject(netID); //this was "ownerBodyId"
			if (!bodyObject) {
				Debug.LogWarning("SyncSomething: bodyObject is null");
				return;
			}
			//Util.PlaySound("Play_drone_repair", bodyObject);

			//Chat.AddMessage("SyncSomething: Adding to kill count on client");
			CharacterBody body = bodyObject.GetComponent<CharacterBody>();
			DronePassiveSkill passiveSkill = bodyObject.GetComponent<DronePassiveSkill>();
			MasterKillCounter masterKillCounter = body.master.GetComponent<MasterKillCounter>();
			if (!masterKillCounter) {
				masterKillCounter = body.master.gameObject.AddComponent<MasterKillCounter>();
			}
			masterKillCounter?.AddKill(passiveSkill);
			//Chat.AddMessage("SyncSomething: Completed!");
		}

		public void Serialize(NetworkWriter writer) {
			writer.Write(netID);
			writer.Write(position);
			writer.Write(number);
		}
	}

	// Messages send parameters, commands don't. Here I just want the server to be able to send kill events to the client to run the below code.
	public class KillCounterComponent : MonoBehaviour, IOnKilledOtherServerReceiver {//, R2API.Networking.Interfaces.INetCommand {
		private CharacterBody body;
		private DronePassiveSkill passiveSkill;
		public MasterKillCounter masterKillCounter;

		// Due to a bug, we must validate that the last ting killed wasn't this ting. Confusing.
		int lastHashCode = -1;

		public void OnHostKill() {
			Debug.Log($"OnKilledOtherServer: OnHostKill() called on host :) ({gameObject.GetHashCode()})");

			//two choices here, first you could have it reset on stage, or you could attach a component to the master, and have this communicate with that. this is the latter
			this.body = base.GetComponent<CharacterBody>();

			if (!passiveSkill) {
				passiveSkill = this.body.GetComponent<DronePassiveSkill>();
			}

			masterKillCounter = body.master.GetComponent<MasterKillCounter>();

			if (!masterKillCounter) {
				masterKillCounter = body.master.gameObject.AddComponent<MasterKillCounter>();
			}

			body.master.GetComponent<MasterKillCounter>()?.AddKill(passiveSkill);
		}

		public void OnKilledOtherServer(DamageReport report) {
			if (!(report.victim && report.attackerMaster)) {
				Debug.LogWarning("OnKilledOtherServer: DamageReport did not have both victim and attackerMaster");
				return;
			}

			// Bug fix
			if (report.victim.GetHashCode() == lastHashCode) {
				return;
			}

			lastHashCode = report.victim.GetHashCode();

			// If the user that made the kill is NOT the host (server), send the command to run this function on their machine.
			if (!report.attackerMaster.hasEffectiveAuthority) {
				// possibly useful things:
				//report.attackerMaster.networkIdentity
				//report.attackerMaster.netId
				//report.attackerMaster.GetNetworkChannel()
				Debug.Log("OnKilledOtherServer: Getting network identity component from attackerBody");
				//NetworkIdentity identity = body.gameObject.GetComponent<NetworkIdentity>();
				NetworkIdentity identity = report.attackerBody.GetComponent<NetworkIdentity>();
				if (!identity) {
					Debug.LogWarning("OnKilledOtherServer: The attackerBody did not have a NetworkIdentity component!");
					return;
				}

				Debug.Log("OnKilledOtherServer: Creating new SyncSomething and sending it");
				new SyncSomething(identity.netId, report.attackerBody.transform.position, 9000).Send(NetworkDestination.Clients);

			} else {
				Debug.Log("OnKilledOtherServer: Calling OnReceived on the host");
				OnHostKill();
			}

		}

		/*public void OnKilledOtherServer(DamageReport report) {

			// Bug fix
			if (report.victim.GetHashCode() == lastHashCode) {
				return;
			}

			lastHashCode = report.victim.GetHashCode();

			// If the user that made the kill is NOT the host (server), send the command to run this function on their machine.
			if (!report.attackerMaster.hasEffectiveAuthority) {
				// possibly useful things:
				//report.attackerMaster.networkIdentity
				//report.attackerMaster.netId
				//report.attackerMaster.GetNetworkChannel()
				this.Send(NetworkDestination.Clients);

			} else {
				OnReceived();
			}
		}*/
	}
}
