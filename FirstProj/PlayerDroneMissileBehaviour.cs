﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using RoR2.Projectile;
using RoR2;

namespace MissileDroneSurvivor.Projectiles {
	// Token: 0x0200060B RID: 1547
	[RequireComponent(typeof(ProjectileTargetComponent))]
	[RequireComponent(typeof(Rigidbody))]
	public class MsIsleMissileController : MonoBehaviour {
		// Token: 0x060025C6 RID: 9670 RVA: 0x0009D944 File Offset: 0x0009BB44
		public void Awake() {
			// This is causing issues!!!!
			// The network server NEEDS to be active when we awaken this component, otherwise it will be a DUMMY
			// Removed this 20-10-2020 18:28 due to it causing multiplayer guests to fire "dumb" inactive missiles
			/*if (!NetworkServer.active) {
				Chat.AddMessage("NetworkServer.active was false (@Awake in MsIsleMissileController)");
				base.enabled = false;
				return;
			}*/

			this.transform = base.transform;
			this.rigidbody = base.GetComponent<Rigidbody>();
			this.torquePID = base.GetComponent<QuaternionPID>();
			this.teamFilter = base.GetComponent<TeamFilter>();
			this.targetComponent = base.GetComponent<ProjectileTargetComponent>();
		}

		public void FixedUpdate() {

			this.timer += Time.fixedDeltaTime;

			if (this.timer < this.giveupTimer) {
				this.rigidbody.velocity = this.transform.forward * this.maxVelocity;
				if (this.targetComponent.target && this.timer >= this.delayTimer) {
					this.rigidbody.velocity = this.transform.forward * (this.maxVelocity + this.timer * this.acceleration);
					Vector3 vector = this.targetComponent.target.position + UnityEngine.Random.insideUnitSphere * this.turbulence - this.transform.position;
					if (vector != Vector3.zero) {
						Quaternion rotation = this.transform.rotation;
						Quaternion targetQuat = Util.QuaternionSafeLookRotation(vector);
						this.torquePID.inputQuat = rotation;
						this.torquePID.targetQuat = targetQuat;
						this.rigidbody.angularVelocity = this.torquePID.UpdatePID();
					}
				}
			}
			if (!this.targetComponent.target) {
				this.targetComponent.target = this.FindTarget();
			} else {
				HealthComponent component = this.targetComponent.target.GetComponent<HealthComponent>();
				if (component && !component.alive) {
					this.targetComponent.target = this.FindTarget();
				}
			}
			if (this.timer > this.deathTimer) {
				UnityEngine.Object.Destroy(base.gameObject);
			}
		}

		// Token: 0x060025C8 RID: 9672 RVA: 0x0009DB38 File Offset: 0x0009BD38
		private Transform FindTarget() {
			// Original Missile Controller code:
			/*this.search.searchOrigin = this.transform.position;
			this.search.searchDirection = this.transform.forward;
			this.search.teamMaskFilter.RemoveTeam(this.teamFilter.teamIndex);
			this.search.RefreshCandidates();
			HurtBox hurtBox = this.search.GetResults().FirstOrDefault<HurtBox>();
			if (hurtBox == null) {
				return null;
			}
			return hurtBox.transform;*/

			this.search.searchOrigin = this.transform.position;
			this.search.searchDirection = this.transform.forward;
			this.search.maxAngleFilter = 70.0f;
			this.search.teamMaskFilter.RemoveTeam(this.teamFilter.teamIndex);
			// This instead sorts by closest to the aim angle
			this.search.sortMode = BullseyeSearch.SortMode.Angle; // = BullseyeSearch.SortMode.DistanceAndAngle;
			this.search.RefreshCandidates();

			HurtBox hurtBox = search.GetResults().FirstOrDefault<HurtBox>();
			if (hurtBox == null) {
				return null;
			}
			return hurtBox.transform;
		}

		// Token: 0x0400209D RID: 8349
		private new Transform transform;

		// Token: 0x0400209E RID: 8350
		private Rigidbody rigidbody;

		// Token: 0x0400209F RID: 8351
		private TeamFilter teamFilter;

		// Token: 0x040020A0 RID: 8352
		private ProjectileTargetComponent targetComponent;

		// Token: 0x040020A1 RID: 8353
		public float maxVelocity;

		// Token: 0x040020A2 RID: 8354
		public float rollVelocity;

		// Token: 0x040020A3 RID: 8355
		public float acceleration;

		// Token: 0x040020A4 RID: 8356
		public float delayTimer;

		// Token: 0x040020A5 RID: 8357
		public float giveupTimer = 8f;

		// Token: 0x040020A6 RID: 8358
		public float deathTimer = 10f;

		// Token: 0x040020A7 RID: 8359
		private float timer;

		// Token: 0x040020A8 RID: 8360
		private QuaternionPID torquePID;

		// Token: 0x040020A9 RID: 8361
		public float turbulence;

		// Token: 0x040020AA RID: 8362
		public float maxSeekDistance = 40f;

		//public float maxAngle = 70.0f;

		// Token: 0x040020AB RID: 8363
		private BullseyeSearch search = new BullseyeSearch();
	}
}
