﻿using BepInEx;
using R2API;
using R2API.Utils;
using R2API.AssetPlus;
using R2API.Networking;
using EntityStates;
using RoR2;
using RoR2.Skills;
using System;
using System.Linq;
using System.Collections.Generic;
//using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

/*
		TODO:
		
		X. Once fire interval reaches a small enough number or numRockets exceeds a certain number, start firing some in parallel
			a. 0.04 should be the min?
			b. Can do with numRockets intdiv/ (0.04 / 0.5)

		\DONE/. New Ability - Special | Nuke
			a. An energy missile like Nova's AntiMatter drop that absorbs all damage it takes and multiplies it by 2.5x, turning into a massive explosion on impact

		X. New Ability - Secondary | Dumbfire Assault

		X. New Ability - Utility/Special | Satchel Charge
			a. Cannot recharge until all satchels have been detonated
			b. Default stock of 1
			c. Pressing ability when all stock is depleted will detonate all the satchels at once
			d. Slight delay before detonation, satchel charges detonate 0.25s after one another, in the order they were placed
			e. Triggers on-kill effects automatically
			f. Huuuuge detonation force
			g. Recharge = 15s?
			h. CAN DAMAGE SELF!!! ?

		X. New Ability - Special | Recruitment Drive
			a. Hire 4 random type drones (gunner, backup, incinerator, missile) that share your items for 20 seconds then die
		
		X. Random ability idea - Reboot & Repair
			a. Shut down - no abilities, no movement, similar to the drone death state
			b. After X seconds, all health is restored, all cooldowns are reset, and all debuffs are removed
			c. Maybe some partial i-frames at the start? or end?
 
	 */


namespace MissileDroneSurvivor {
	[R2APISubmoduleDependency(nameof(SurvivorAPI), nameof(PrefabAPI), nameof(LoadoutAPI), nameof(LanguageAPI), nameof(NetworkingAPI))]
	[BepInDependency("com.bepis.r2api", BepInDependency.DependencyFlags.HardDependency)]
	[BepInPlugin("com.hogynmelyn.msisle", "Ms Isle", "1.2.0")]
	public class MissileDroneMod : BaseUnityPlugin {

		// todo remove static keywords pls
		public static GameObject msIsleCharacter; // Added static keyword.
		public static CharacterBody bodyComponent; // 12-10-2020-13:34 Moved this to a global var and added public static keywords.
		public SkillLocator skillLocator; // REMOVED static keyword 12-10-2020-13:34
		DronePassiveSkill dronePassiveManager;

		const int numRocketsBase = 4;
		const float rocketFireIntervalBase = 0.5f;
		const float missileRecharge = 2.0f; // in seconds
		GameObject missilePrefab = null;

		public void Awake() {
			Debug.Log(">|Loaded Ms. Isle survivor mod! :)");

			//SpoofMultiplayer();

			RegisterStates(); // register our skill entitystates for networking

			SetupCharacterBody();

			SetupPreSkills();

			SetupPassive();

			SetupPrimary();

			SetupSecondary();

			SetupUtility();

			SetupSpecial();

			Skins.RegisterSkins();
		}

		void SpoofMultiplayer() {
			Debug.Log("Allowing self-connections for network testing...");
			On.RoR2.Networking.GameNetworkManager.OnClientConnect += (self, user, t) => { };
			Debug.Log("OnClientConnect behaviour overwritten successfully!");
		}

		void SetupCharacterBody() {
			GameObject missileDroneBodyOriginal = Resources.Load<GameObject>("Prefabs/CharacterBodies/MissileDroneBody");

			msIsleCharacter = missileDroneBodyOriginal.InstantiateClone("MissileDroneSurvivor");

			msIsleCharacter.GetComponent<NetworkIdentity>().localPlayerAuthority = true;


			//Note; if your character cannot interact with things, play around with the following value after uncommenting them
			//KinematicCharacterController.KinematicCharacterMotor kinematicCharacterController = msIsleCharacter.GetComponentInChildren<KinematicCharacterController.KinematicCharacterMotor>();
			//gameObject.transform.localScale *= 0.25f;
			msIsleCharacter.GetComponent<CharacterBody>().aimOriginTransform.Translate(new Vector3(0, 0.5f, 0));
			//kinematicCharacterController.SetCapsuleDimensions(kinematicCharacterController.Capsule.radius * 0.25f, kinematicCharacterController.Capsule.height * 0.25f, -3f);
			


			// ##### Add equipment usability #####
			EquipmentSlot equipmentSlot = msIsleCharacter.AddComponent<EquipmentSlot>();
			// ###################################


			bodyComponent = msIsleCharacter.GetComponent<CharacterBody>();
			bodyComponent.baseDamage = 10f;
			bodyComponent.levelDamage = 1.0f; // was 0
			bodyComponent.baseCrit = 10f;
			bodyComponent.levelCrit = 0.0f;
			bodyComponent.baseMaxHealth = 50f;
			bodyComponent.levelMaxHealth = 15f;
			bodyComponent.baseArmor = 0f;
			bodyComponent.baseRegen = 1.5f;
			bodyComponent.levelRegen = 0.25f;
			bodyComponent.baseMoveSpeed = 10f;
			bodyComponent.levelMoveSpeed = 0.1f; //4.5 is insane
			bodyComponent.baseAttackSpeed = 1f;
			bodyComponent.levelAttackSpeed = 0f;///0.03f; //0.05f; //0.1
			bodyComponent.baseMaxShield = 0;
			bodyComponent.levelMaxShield = 0f;
			bodyComponent.name = "Ms. Isle";
			bodyComponent.portraitIcon = Resources.Load<Texture>("Textures/Bodyicons/texMissileDroneIcon");
			bodyComponent.baseJumpCount = 1;

			// Add in the "sprint in any direction" bit/enum from CharacterBody
			bodyComponent.bodyFlags = bodyComponent.bodyFlags | CharacterBody.BodyFlags.SprintAnyDirection;

			// Add the drone-sprint component that allows us to contextually sprint without user input
			dronePassiveManager = msIsleCharacter.AddComponent<DronePassiveSkill>();

			msIsleCharacter.GetComponent<CharacterBody>().preferredPodPrefab = Resources.Load<GameObject>("Prefabs/CharacterBodies/toolbotbody").GetComponent<CharacterBody>().preferredPodPrefab;

			//LanguageAPI.Add("MSISLECHAR_DESCRIPTION", "Ms. Isle is a missile-firing, war-crime-committing, glass-cannon type survivor!" + Environment.NewLine);
			LanguageAPI.Add("MSISLECHAR_DESCRIPTION", "Ms. Isle is a glass-cannon Missile Drone, capable of <i>extremely</i> high damage output and unique evasive flight-based movement, but with minimal health. <style=cSub>\r\n\r\n< ! > Fly-By serves as Ms Isle\u2019s sprint mechanism \u2013 use it to navigate stages more quickly, and then take to the skies to evade enemy attacks!\r\n\r\n< ! > Missile Barrage always fires its swarm of rockets over <i>1 second</i>, but the number of rockets it fires is determined by Ms Isle\u2019s overall fire-rate.\r\n\r\n< ! > Cumulative Warhead absorbs all damage dealt to it by you and your allies! Fire it into a group of enemies from a distance and focus your fire to increase its damage multiplier up to <style=cIsDamage>many times its base damage</style>!\r\n\r\n< ! > Pack Hound missiles will target whatever is closest, <i>including</i> your Cumulative Warhead!</style>");
			LanguageAPI.Add("MSISLECHAR_NAME", "Ms. Isle");
			LanguageAPI.Add("MSISLECHAR_DISPLAYNAME", "Ms. Isle");
			LanguageAPI.Add("MSISLE_SUBTITLE", "Missile Drone");

			// ## Set up the Display Prefab ##
			GameObject displayPrefab = PrefabAPI.InstantiateClone(bodyComponent.GetComponent<ModelLocator>().modelBaseTransform.gameObject, "MissileDroneSurvivorDisplay");
			displayPrefab.AddComponent<NetworkIdentity>();
			displayPrefab.transform.localScale *= 0.75f;
			Transform displayArmature = displayPrefab.transform.Find("mdlMissileDrone").Find("MissileDroneArmature");
			Transform mdlMissileDrone = displayPrefab.transform.Find("mdlMissileDrone");
			mdlMissileDrone.gameObject.AddComponent<DroneDisplaySpoofer>();
			//////////////////////////////////

			SurvivorDef survivorDef = new SurvivorDef {
				name = "MSISLECHAR_NAME",
				bodyPrefab = msIsleCharacter,
				displayPrefab = displayPrefab,//gameObject,
				unlockableName = "",
				displayNameToken = "MSISLECHAR_DISPLAYNAME",
				descriptionToken = "MSISLECHAR_DESCRIPTION",
				primaryColor = new Color(0.8039216f, 0.482352942f, 0.843137264f),
				outroFlavorToken = "MSISLE_SUBTITLE"
			};

			Debug.Log("    >|Created survivor definition!");

			SurvivorAPI.AddSurvivor(survivorDef);
			Debug.Log("    >|Added survivor to API!");

			Debug.Log("    >|Adding msIsleCharacter GameObject to the body catalogue...");

			BodyCatalog.getAdditionalEntries += delegate (List<GameObject> list) {
				list.Add(msIsleCharacter);
			};
			Debug.Log("    >|Finished setting up character body!");
		}

		void SetupPreSkills() {
			Debug.Log("        >|Setting up skills...");

			Debug.Log("        >|Setting the global SkillLocator var to the SkillLocator component on the CharacterBody!");
			skillLocator = msIsleCharacter.GetComponent<SkillLocator>();

			Debug.Log("        >|Deleting existing GenericSkill components attached...");
			foreach (GenericSkill preexistingSkill in msIsleCharacter.GetComponents<GenericSkill>()) {
				Destroy(preexistingSkill);
				Debug.Log("            >|Deleted a GenericSkill component!");
			}


			skillLocator.primary = bodyComponent.gameObject.AddComponent<GenericSkill>();
			Debug.Log("        >|Added primary skill slot!");
			var primaryFamily = ScriptableObject.CreateInstance<SkillFamily>();         // Instance a new family
			skillLocator.primary.SetFieldValue("_skillFamily", primaryFamily);          // Force the Skill Locator to use this new family for the primary slot.
			Debug.Log("        >|Added the primary skill family!");

			skillLocator.secondary = bodyComponent.gameObject.AddComponent<GenericSkill>();
			Debug.Log("        >|Added secondary skill slot!");
			var secondaryFamily = ScriptableObject.CreateInstance<SkillFamily>();         // Instance a new family
			skillLocator.secondary.SetFieldValue("_skillFamily", secondaryFamily);          // Force the Skill Locator to use this new family for the primary slot.
			Debug.Log("        >|Added the secondary skill family!");


			skillLocator.utility = bodyComponent.gameObject.AddComponent<GenericSkill>();
			Debug.Log("        >|Added utility skill slot!");
			var utilityFamily = ScriptableObject.CreateInstance<SkillFamily>();         // Instance a new family
			skillLocator.utility.SetFieldValue("_skillFamily", utilityFamily);          // Force the Skill Locator to use this new family for the primary slot.
			Debug.Log("        >|Added the utility skill family!");


			skillLocator.special = bodyComponent.gameObject.AddComponent<GenericSkill>();
			Debug.Log("        >|Added special skill slot!");
			var specialFamily = ScriptableObject.CreateInstance<SkillFamily>();         // Instance a new family
			skillLocator.special.SetFieldValue("_skillFamily", specialFamily);          // Force the Skill Locator to use this new family for the primary slot.
			Debug.Log("        >|Added the special skill family!");


			// Register it with the API!
			LoadoutAPI.AddSkillFamily(primaryFamily);
			LoadoutAPI.AddSkillFamily(secondaryFamily);
			LoadoutAPI.AddSkillFamily(utilityFamily);
			LoadoutAPI.AddSkillFamily(specialFamily);
			Debug.Log("        >|Registered all Skill Families with the API!");
		}

		void RegisterStates() {

			//NetworkingAPI.RegisterCommandType<KillCounterComponent>();
			NetworkingAPI.RegisterMessageType<SyncSomething>();

			// register the entitystates for networking reasons
			LoadoutAPI.AddSkill(typeof(MissileDroneSurvivor.MsIsleEntityStates.MissileBarrage));
			LoadoutAPI.AddSkill(typeof(MissileDroneSurvivor.MsIsleEntityStates.PackHounds));
			LoadoutAPI.AddSkill(typeof(MissileDroneSurvivor.MsIsleEntityStates.DroneBoostAbility));
			LoadoutAPI.AddSkill(typeof(MissileDroneSurvivor.MsIsleEntityStates.NukeAbility));
			LoadoutAPI.AddSkill(typeof(MissileDroneSurvivor.MsIsleEntityStates.TestPrimarySkill));
			Debug.Log("    >|Registered networked skill states with LoadoutAPI!");
		}

		void SetupPassive() {
			// Currently the passive is handled in DroneSprint, thus this is simply cosmetic.

			LanguageAPI.Add("MSISLE_PASSIVE_NAME", "Fly-by");
			LanguageAPI.Add("MSISLE_PASSIVE_DESCRIPTION", "Ms. Isle can <style=cIsUtility>fly</style> in any direction, and <style=cIsUtility>sprints</style> automatically when close to terrain. <style=cIsUtility>Jumping</style> is replaced with <style=cIsUtility>Overclocked Drives.</style>");

			skillLocator.passiveSkill.enabled = true;
			skillLocator.passiveSkill.skillNameToken = "MSISLE_PASSIVE_NAME";
			skillLocator.passiveSkill.skillDescriptionToken = "MSISLE_PASSIVE_DESCRIPTION";
			//skillLocator.passiveSkill.icon = Resources.Load<Sprite>("textures/miscicons/texSprintIcon");
			skillLocator.passiveSkill.icon = Resources.Load<Sprite>("textures/miscicons/texDroneIconOutlined");
			Debug.Log("    >|Passive ability set up successfuly!");
		}

		void SetupPrimary() {
			Debug.Log("    >|Adding Primary ability...");

			//LanguageAPI.Add("MSISLE_1_MISSILEBARRAGE_NAME", "Missile Barrage");
			LanguageAPI.Add("MSISLE_1_MISSILEBARRAGE_NAME", "AtG Misike Mk.II");
			LanguageAPI.Add("MSISLE_1_MISSILEBARRAGE_DESCRIPTION", "<style=cIsUtility>Agile</style>. Fire a barrage of missiles for <style=cIsDamage>6x100% Damage</style>. Each missile explodes in a small radius. Fire-rate bonuses increase the number of missiles in the barrage.");

			Debug.Log("        >|Creating primary skill definition...");
			var primarySkillDef = ScriptableObject.CreateInstance<SkillDef>();
			primarySkillDef.activationState = new SerializableEntityStateType(typeof(MissileDroneSurvivor.MsIsleEntityStates.MissileBarrage));
			primarySkillDef.activationStateMachineName = "Weapon";
			primarySkillDef.baseMaxStock = 1;
			primarySkillDef.baseRechargeInterval = 2f;
			primarySkillDef.beginSkillCooldownOnSkillEnd = false; //false; Changed this to true in in lieu of the stock changes! // changed back lul
			primarySkillDef.canceledFromSprinting = false;
			primarySkillDef.fullRestockOnAssign = true;
			primarySkillDef.interruptPriority = InterruptPriority.Any;
			primarySkillDef.isBullets = false;
			primarySkillDef.isCombatSkill = true;
			primarySkillDef.mustKeyPress = false;
			primarySkillDef.noSprint = false;
			primarySkillDef.rechargeStock = 1;
			primarySkillDef.requiredStock = 1;
			primarySkillDef.shootDelay = 1f; // does nothing if isBullets = false
			primarySkillDef.stockToConsume = 1;
			primarySkillDef.icon = Resources.Load<Sprite>("Textures/Achievementicons/texEngiClearTeleporterWithZeroMonstersIcon"); //texCaptainVisitSeveralStagesIcon works too
			primarySkillDef.skillDescriptionToken = "MSISLE_1_MISSILEBARRAGE_DESCRIPTION";
			primarySkillDef.skillName = "MSISLE_1_MISSILEBARRAGE_NAME";
			primarySkillDef.skillNameToken = "MSISLE_1_MISSILEBARRAGE_NAME";
			primarySkillDef.keywordTokens = new string[] { "KEYWORD_AGILE" };
			//KEYWORD_AGILE, KEYWORD_STUNNING, KEYWORD_EXPOSE, KEYWORD_HEAVY, KEYWORD_RAPID_REGEN, KEYWORD_POISON, KEYWORD_SHOCKING, KEYWORD_PERCENT_HP, KEYWORD_SONIC_BOOM, KEYWORD_WEAK

			Debug.Log("        >|Registering skill definition with the API...");
			// This adds our skillDef. If you don't do this, the skill will NOT work!
			LoadoutAPI.AddSkillDef(primarySkillDef);


			Debug.Log("        >|Setting up the projectile...");
			// Set up the projectile
			missilePrefab = Resources.Load<GameObject>("Prefabs/Projectiles/MissileProjectile");
			missilePrefab = missilePrefab.InstantiateClone("Prefabs/MissileDroneMod/MissilePrefab");

			if (missilePrefab != null) {
				RoR2.Projectile.MissileController missileController = missilePrefab.GetComponent<RoR2.Projectile.MissileController>();

				RoR2.Projectile.ProjectileDamage projectileDamage = missilePrefab.GetComponent<RoR2.Projectile.ProjectileDamage>();
				projectileDamage.force = 20.0f; //100.0f;//30.0f; // EDIT - 100 proved to be absolutely insane once you get up to 20+ missiles; moved down to 20.

				RoR2.Projectile.ProjectileController projectileController = missilePrefab.GetComponent<RoR2.Projectile.ProjectileController>();
				projectileController.procCoefficient = 1.0f;

				// Remove the single-target damage component (moving to a radial attack)
				Destroy(missilePrefab.GetComponent<RoR2.Projectile.ProjectileSingleTargetImpact>());

				// Add an explosion radius
				RoR2.Projectile.ProjectileImpactExplosion projectileImpactExplosion = missilePrefab.AddComponent<RoR2.Projectile.ProjectileImpactExplosion>();
				//projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/Effects/Impacteffects/EngiHarpoonExplosion");
				projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/Effects/Omnieffect/OmniExplosionVFXCommandoGrenade");
				projectileImpactExplosion.destroyOnEnemy = true;
				projectileImpactExplosion.destroyOnWorld = true;
				projectileImpactExplosion.timerAfterImpact = false;
				projectileImpactExplosion.falloffModel = BlastAttack.FalloffModel.Linear;
				projectileImpactExplosion.lifetime = 10.0f;
				projectileImpactExplosion.lifetimeAfterImpact = 0;
				projectileImpactExplosion.lifetimeRandomOffset = 0;
				projectileImpactExplosion.blastRadius = 7;
				projectileImpactExplosion.blastDamageCoefficient = 1;
				projectileImpactExplosion.blastProcCoefficient = 1;
				projectileImpactExplosion.blastAttackerFiltering = RoR2.AttackerFiltering.Default;
				projectileImpactExplosion.bonusBlastForce = new Vector3(0, 0, 750);//(0, 0, 750, 0, 0, 0);
				projectileImpactExplosion.fireChildren = false;
				//projectileImpactExplosion.childrenProjectilePrefab =;
				projectileImpactExplosion.childrenCount = 0;
				projectileImpactExplosion.childrenDamageCoefficient = 0;
				projectileImpactExplosion.minAngleOffset = Vector3.zero;//(0, 0, 0, 0, 0, 0);
				projectileImpactExplosion.maxAngleOffset = Vector3.zero;//(0, 0, 0, 0, 0, 0);
				projectileImpactExplosion.transformSpace = RoR2.Projectile.ProjectileImpactExplosion.TransformSpace.World;
				//projectileImpactExplosion.projectileHealthComponent =;
				//projectileImpactExplosion.AuthorityEffect;

				if (missilePrefab != null) {
					MissileDroneSurvivor.MsIsleEntityStates.MissileBarrage.projectilePrefab = missilePrefab;
				}

				MissileDroneSurvivor.MsIsleEntityStates.MissileBarrage.baseFireInterval = bodyComponent.baseAttackSpeed;

				// Copy the properties across + set our own
				Debug.Log("    >|Adding MsIsleMissileController component and setting/copying properties...");
				Projectiles.MsIsleMissileController newMissileController = missilePrefab.AddComponent<Projectiles.MsIsleMissileController>();
				newMissileController.acceleration = 9.0f;
				newMissileController.deathTimer = missileController.deathTimer;
				newMissileController.delayTimer = missileController.delayTimer;//0.5f;
				newMissileController.giveupTimer = missileController.giveupTimer;
				newMissileController.maxSeekDistance = missileController.maxSeekDistance;
				//newMissileController.maxAngle = 70.0f; // ?
				newMissileController.maxVelocity = 50.0f;
				newMissileController.rollVelocity = missileController.rollVelocity;
				newMissileController.tag = missileController.tag;
				newMissileController.turbulence = 0.05f;

				// DONT DO THIS YOU FFUCKING SMOOTHBRAINED RETARD
				// JESUS CHRIST! A WHOLE DAY WASTED 
				//newMissileController.Awake();

				//Debug.Log("Copied missile controller properties and altered new ones.");
				Destroy(missileController);
				//Debug.Log("Deleted old missile controller...");

				if (!missilePrefab.GetComponent<NetworkIdentity>()) {
					missilePrefab.AddComponent<NetworkIdentity>();
				}
				ProjectileCatalog.getAdditionalEntries += list => list.Add(missilePrefab);
				missilePrefab.RegisterNetworkPrefab("Prefabs/MissileDroneMod/MissilePrefab");
			}



			// OLD SHITTY CODE
			/*			var skillFamily = skillLocator.primary.skillFamily;
						skillFamily.variants[0] = new SkillFamily.Variant {
							skillDef = primarySkillDef,
							unlockableName = "",
							viewableNode = new ViewablesCatalog.Node(primarySkillDef.skillNameToken, false, null)
						};
			*/


			// MOVED THIS TO THE PRE-SKILL SETUP
			/*
			Debug.Log("        >|Creating a new Skill Family to replace the default primary ability family...");
			// Create a new family, in this case to avoid editing the existing family (Missile Drone's default abilities)
			var primaryFamily = ScriptableObject.CreateInstance<SkillFamily>();         // Instance a new family
			Debug.Log("        >|Registering new skill family with the API...");
			LoadoutAPI.AddSkillFamily(primaryFamily);                                   // Register it with the API
			Debug.Log("        >|Forcibly setting _skillFamily to primaryFamily...");
			skillLocator.primary.SetFieldValue("_skillFamily", primaryFamily);          // Force the Skill Locator to use this new family for the primary slot.
			*/


			/*Debug.Log("        >|Adding skill variant to the primary family...");
			skillLocator.primary.skillFamily.variants = new SkillFamily.Variant[1]; // substitute 1 for the number of skill variants you are implementing
			skillLocator.primary.skillFamily.variants[0] = new SkillFamily.Variant {
				skillDef = primarySkillDef,
				unlockableName = "",
				viewableNode = new ViewablesCatalog.Node(primarySkillDef.skillNameToken, false, null)   // Adds the skill as a viewable node in the character select & loadout screens
			};*/

			Debug.Log("        >|Adding skill variant to the primary family...");
			skillLocator.primary.skillFamily.variants = new SkillFamily.Variant[] {
				new SkillFamily.Variant {
					skillDef = primarySkillDef,
					unlockableName = "",
					viewableNode = new ViewablesCatalog.Node(primarySkillDef.skillNameToken, false, null)   // Adds the skill as a viewable node in the character select & loadout screens
				}
			};

			Debug.Log("    >|Added primary ability!");
		}

		void SetupSecondary() {
			Debug.Log("    >|Adding Secondary ability...");
			LanguageAPI.Add("MSISLE_2_BARRAGE_NAME", "Pack Hounds");
			LanguageAPI.Add("MSISLE_2_BARRAGE_DESCRIPTION", "<style=cIsUtility>Agile.</style> Fire a barrage of auto-targeting pack-hound missiles for <style=cIsDamage>400% damage</style>. <i><b><style=cIsUtility>Charges for this skill are gained by kills</style></b></i>.");

			SkillDef secondarySkillDef = ScriptableObject.CreateInstance<SkillDef>();
			secondarySkillDef.activationState = new SerializableEntityStateType(typeof(MissileDroneSurvivor.MsIsleEntityStates.PackHounds));
			secondarySkillDef.activationStateMachineName = "Weapon";
			secondarySkillDef.baseMaxStock = 50;
			secondarySkillDef.baseRechargeInterval = 0.0f; //15
			secondarySkillDef.beginSkillCooldownOnSkillEnd = true;
			secondarySkillDef.canceledFromSprinting = false;//false;// true;
			secondarySkillDef.fullRestockOnAssign = false;
			secondarySkillDef.interruptPriority = InterruptPriority.Any;
			secondarySkillDef.isBullets = false;//true; // false
			secondarySkillDef.isCombatSkill = true;
			secondarySkillDef.mustKeyPress = true;
			secondarySkillDef.noSprint = false;
			secondarySkillDef.rechargeStock = 0;
			secondarySkillDef.requiredStock = 1;
			secondarySkillDef.shootDelay = 0.5f;//2.0f;//0.5f;
			secondarySkillDef.stockToConsume = 1;
			secondarySkillDef.icon = Resources.Load<Sprite>("Textures/bufficons/texBuffFullCritIcon");
			//specialSkillDef.icon = Resources.Load<Sprite>("Textures/itemicons/texMissileRackIcon");
			secondarySkillDef.skillDescriptionToken = "MSISLE_2_BARRAGE_DESCRIPTION";
			secondarySkillDef.skillName = "MSISLE_2_BARRAGE_NAME";
			secondarySkillDef.skillNameToken = "MSISLE_2_BARRAGE_NAME";
			secondarySkillDef.keywordTokens = new string[] { "KEYWORD_AGILE" };
			//KEYWORD_AGILE, KEYWORD_STUNNING, KEYWORD_EXPOSE, KEYWORD_HEAVY, KEYWORD_RAPID_REGEN, KEYWORD_POISON, KEYWORD_SHOCKING, KEYWORD_PERCENT_HP, KEYWORD_SONIC_BOOM, KEYWORD_WEAK
			LoadoutAPI.AddSkillDef(secondarySkillDef);

			skillLocator.secondary.skillFamily.variants = new SkillFamily.Variant[1]; // substitute 1 for the number of skill variants you are implementing
			skillLocator.secondary.skillFamily.variants[0] = new SkillFamily.Variant {
				skillDef = secondarySkillDef,
				unlockableName = "",
				viewableNode = new ViewablesCatalog.Node(secondarySkillDef.skillNameToken, false, null)   // Adds the skill as a viewable node in the character select & loadout screens
			};

			// Assign the projectile
			//MsIsleEntityStates.PackHounds.projectilePrefab = Resources.Load<GameObject>("Prefabs/Projectiles/MissileProjectile").InstantiateClone("PackHoundMissile");
			MsIsleEntityStates.PackHounds.projectilePrefab = Resources.Load<GameObject>("Prefabs/Projectiles/MissileProjectile").InstantiateClone("Prefabs/MissileDroneMod/PackHoundMissile");

			// Fuck hopoo's networking code. This allows non-hosts to shoot missiles because apparently that was disabled by default.
			Projectiles.MissileControllerNotFucked missileControllerNotFucked = MsIsleEntityStates.PackHounds.projectilePrefab.AddComponent<Projectiles.MissileControllerNotFucked>();
			missileControllerNotFucked.CannibaliseFucked();

			// Edit it such that they don't break on world contact
			MsIsleEntityStates.PackHounds.projectilePrefab.GetComponent<RoR2.Projectile.ProjectileSingleTargetImpact>().destroyOnWorld = false;
			// Sanity Check™
			if (!MsIsleEntityStates.PackHounds.projectilePrefab.GetComponent<NetworkIdentity>()) {
				MsIsleEntityStates.PackHounds.projectilePrefab.AddComponent<NetworkIdentity>();
			}
			ProjectileCatalog.getAdditionalEntries += list => list.Add(MsIsleEntityStates.PackHounds.projectilePrefab);
			MsIsleEntityStates.PackHounds.projectilePrefab.RegisterNetworkPrefab("Prefabs/MissileDroneMod/PackHoundMissile");

			skillLocator.secondary.stock = 0;

			// Important step here; register this skill with the DronePassiveSkill component to manage its stock count and usage.
			dronePassiveManager.packHoundsSkill = skillLocator.secondary;
			//dronePassiveManager.packHoundsSkillDef = secondarySkillDef; // nope

			Debug.Log("    >|Added secondary ability!");
		}

		void SetupUtility() {
			Debug.Log("    >|Adding Utility ability...");
			LanguageAPI.Add("MSISLE_3_DRONEBOOST_NAME", "Overclocked Drives");
			LanguageAPI.Add("MSISLE_3_DRONEBOOST_DESCRIPTION", "Enter a brief <style=cIsUtility>omnidirectional boost</style>. <style=cIsUtility>Replenishes all charges</style> when near terrain. <b>Bonuses to jump stats affect this ability instead!</b>");

			SkillDef utilitySkillDef = ScriptableObject.CreateInstance<SkillDef>();
			utilitySkillDef.activationState = new SerializableEntityStateType(typeof(MissileDroneSurvivor.MsIsleEntityStates.DroneBoostAbility));
			utilitySkillDef.activationStateMachineName = "Weapon"; //fuck this var
			utilitySkillDef.baseMaxStock = 1;
			utilitySkillDef.baseRechargeInterval = 0.0f;//100.0f;
			utilitySkillDef.beginSkillCooldownOnSkillEnd = true; //added to fix infinite boost bug
			utilitySkillDef.canceledFromSprinting = false;// true;
			utilitySkillDef.fullRestockOnAssign = false;//true; //added to fix infinite boost bug
			utilitySkillDef.interruptPriority = InterruptPriority.Any; // Was Any. Changed to Death. Didn't work.
			utilitySkillDef.isBullets = false;//true;
			utilitySkillDef.isCombatSkill = false;
			utilitySkillDef.mustKeyPress = true;
			utilitySkillDef.noSprint = false;
			utilitySkillDef.rechargeStock = 0;
			utilitySkillDef.requiredStock = 1;
			utilitySkillDef.dontAllowPastMaxStocks = true; //added to fix infinite boost bug
			utilitySkillDef.shootDelay = 8.0f;//2.0f;//0.5f;
			utilitySkillDef.stockToConsume = 1;
			utilitySkillDef.icon = Resources.Load<Sprite>("Textures/achievementicons/texDiscover5EquipmentIcon");
			utilitySkillDef.skillDescriptionToken = "MSISLE_3_DRONEBOOST_DESCRIPTION";
			utilitySkillDef.skillName = "MSISLE_3_DRONEBOOST_NAME";
			utilitySkillDef.skillNameToken = "MSISLE_3_DRONEBOOST_NAME";
			//secondarySkillDef.keywordTokens = new string[] { "KEYWORD_AGILE" };
			//KEYWORD_AGILE, KEYWORD_STUNNING, KEYWORD_EXPOSE, KEYWORD_HEAVY, KEYWORD_RAPID_REGEN, KEYWORD_POISON, KEYWORD_SHOCKING, KEYWORD_PERCENT_HP, KEYWORD_SONIC_BOOM, KEYWORD_WEAK
			LoadoutAPI.AddSkillDef(utilitySkillDef);

			skillLocator.utility.skillFamily.variants = new SkillFamily.Variant[1]; // substitute 1 for the number of skill variants you are implementing
			skillLocator.utility.skillFamily.variants[0] = new SkillFamily.Variant {
				skillDef = utilitySkillDef,
				unlockableName = "",
				viewableNode = new ViewablesCatalog.Node(utilitySkillDef.skillNameToken, false, null)   // Adds the skill as a viewable node in the character select & loadout screens
			};

			//skillLocator.secondary.stock = 0;

			// Important step here; register this skill with the DronePassiveSkill component to manage its stock count and usage.
			dronePassiveManager.utilitySkill = skillLocator.utility;
			dronePassiveManager.utilitySkillDef = utilitySkillDef;
			//			MsIsleEntityStates.DroneBoostAbility.fauxJetLeft = jetEffectLeft;
			//			MsIsleEntityStates.DroneBoostAbility.fauxJetRight = jetEffectRight;

			Debug.Log("    >|Added secondary ability!");
		}

		void SetupSpecial() {
			Debug.Log("    >|Adding Special ability...");
			LanguageAPI.Add("MSISLE_4_NUKE_NAME", "Cumulative Warhead");
			LanguageAPI.Add("MSISLE_4_NUKE_DESCRIPTION", "<style=cIsUtility>Stunning</style>. Fire a slow-moving rocket for <style=cIsDamage>500% damage</style> that <style=cIsDamage>absorbs damage</style> and releases it in a <style=cIsDamage>huge detonation</style> on impact.");

			SkillDef specialSkillDef = ScriptableObject.CreateInstance<SkillDef>();
			specialSkillDef.activationState = new SerializableEntityStateType(typeof(MissileDroneSurvivor.MsIsleEntityStates.NukeAbility));
			specialSkillDef.activationStateMachineName = "Weapon";
			specialSkillDef.baseMaxStock = 1;
			specialSkillDef.baseRechargeInterval = 35f; //15
			specialSkillDef.beginSkillCooldownOnSkillEnd = true;
			specialSkillDef.canceledFromSprinting = false;// true;
			specialSkillDef.fullRestockOnAssign = true;
			specialSkillDef.interruptPriority = InterruptPriority.Any;
			specialSkillDef.isBullets = true; // false
			specialSkillDef.isCombatSkill = false;
			specialSkillDef.mustKeyPress = true;
			specialSkillDef.noSprint = false;//true;
			specialSkillDef.rechargeStock = 1;
			specialSkillDef.requiredStock = 1;
			specialSkillDef.shootDelay = 0.5f;//2.0f;//0.5f;
			specialSkillDef.stockToConsume = 1;
			specialSkillDef.icon = Resources.Load<Sprite>("Textures/Achievementicons/texObtainArtifactBombIcon");
			specialSkillDef.skillDescriptionToken = "MSISLE_4_NUKE_DESCRIPTION";
			specialSkillDef.skillName = "MSISLE_4_NUKE_NAME";
			specialSkillDef.skillNameToken = "MSISLE_4_NUKE_NAME";
			specialSkillDef.keywordTokens = new string[] { "KEYWORD_STUNNING" };
			//KEYWORD_AGILE, KEYWORD_STUNNING, KEYWORD_EXPOSE, KEYWORD_HEAVY, KEYWORD_RAPID_REGEN, KEYWORD_POISON, KEYWORD_SHOCKING, KEYWORD_PERCENT_HP, KEYWORD_SONIC_BOOM, KEYWORD_WEAK
			SetupNukeProjectile();
			LoadoutAPI.AddSkillDef(specialSkillDef);

			// Add the Special (R) ability slot to the character body, as the Drone doesn't start with a Special by default.
			//skillLocator.special = bodyComponent.gameObject.AddComponent<GenericSkill>();
			// ^^^^ WAS MOVED TO PRE-SKILL SETUP

			skillLocator.special.skillFamily.variants = new SkillFamily.Variant[1]; // substitute 1 for the number of skill variants you are implementing
			skillLocator.special.skillFamily.variants[0] = new SkillFamily.Variant {
				skillDef = specialSkillDef,
				unlockableName = "",
				viewableNode = new ViewablesCatalog.Node(specialSkillDef.skillNameToken, false, null)   // Adds the skill as a viewable node in the character select & loadout screens
			};

			Debug.Log("    >|Added special ability!");
		}

		private void SetupNukeProjectile() {
			// Clone the default vagrant projectile
			GameObject nukePrefabOriginal = Resources.Load<GameObject>("Prefabs/Projectiles/VagrantTrackingBomb");
			GameObject nukePrefab = nukePrefabOriginal.InstantiateClone("Prefabs/MissileDroneMod/NukePrefabTestVagrant");

			HealthComponent healthComponent = nukePrefab.GetComponent<HealthComponent>();
			healthComponent.health = 100000;
			healthComponent.body.baseMaxHealth = 100000;

			//nukePrefab.RegisterNetworkPrefab("C:MainClass.cs", "Prefabs/MissileDroneMod/NukePrefabTestVagrant", 579); // This function can also take the source code's filename and the line number, used to generate the unique network ID hash.


			//CharacterBody nukeBody = nukePrefab.GetComponent<CharacterBody>();

			// This copies the missile "ghost" (visual)
			/*
			GameObject defaultMissilePrefab = Resources.Load<GameObject>("Prefabs/Projectiles/MissileProjectile");//.InstantiateClone("CLONEDMISSILE");
			RoR2.Projectile.ProjectileController nukeProjectileController = nukePrefab.GetComponent<RoR2.Projectile.ProjectileController>();
			nukeProjectileController.ghostPrefab = defaultMissilePrefab.GetComponent<RoR2.Projectile.ProjectileController>().ghostPrefab;
			*/

			// This grabs a unique "ghost" from the ghost folder
			RoR2.Projectile.ProjectileController nukeProjectileController = nukePrefab.GetComponent<RoR2.Projectile.ProjectileController>();
			//nukeProjectileController.ghostPrefab = Resources.Load<GameObject>("Prefabs/projectileghosts/EngiHarpoonGhost");					//safe, unscaled and unedited
			//nukeProjectileController.ghostPrefab = Resources.Load<GameObject>("Prefabs/projectileghosts/EngiHarpoonGhost").InstantiateClone("MsIsleNukeGhost");
			GameObject nukeGhost = Resources.Load<GameObject>("Prefabs/projectileghosts/EngiHarpoonGhost").InstantiateClone("Prefabs/MissileDroneMod/MsIsleNukeGhost");
			if (!nukeGhost.GetComponent<NetworkIdentity>()) {
				nukeGhost.AddComponent<NetworkIdentity>();
			}
			nukeGhost.RegisterNetworkPrefab("Prefabs/MissileDroneMod/MsIsleNukeGhost");
			nukeGhost.transform.localScale *= 7.5f; // BIG BOYS - was previously 5.0f. 9 is slightly toooo big
			nukeProjectileController.ghostPrefab = nukeGhost;

			{
				RoR2.Projectile.ProjectileSimple ps = nukePrefab.GetComponent<RoR2.Projectile.ProjectileSimple>();
				ps.lifetime = 99;
				ps.velocity = 10;
			}

			// Possible ghosts that might look nice
			// RocketGhost
			// MissileGhost
			// PaladinRocketGhost
			// MicroMissileGhost
			// EngiHarpoonGhost

			DamageAccumulator damageAccumulator = nukePrefab.AddComponent<DamageAccumulator>();
			//damageAccumulator.maxHP = 9999999;

			RoR2.Projectile.ProjectileImpactExplosion projectileImpactExplosion = nukePrefab.GetComponent<RoR2.Projectile.ProjectileImpactExplosion>();
			projectileImpactExplosion.blastRadius = 5.0f;
			projectileImpactExplosion.blastDamageCoefficient = 0.25f;
			// NEW! Added falloff to the nuke attack (Insane otherwise).
			projectileImpactExplosion.falloffModel = BlastAttack.FalloffModel.Linear;
			projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/effects/impacteffects/BootShockwave");                 // pretty cool green n squares plasma explosion

			//projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/effects/impacteffects/CaptainAirstrikeImpact1");		// Looks decent! A little sparse but good.

			//projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/effects/impacteffects/ExplosionGolem");				//too small, nice fx tho

			//projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/effects/impacteffects/ExplosionVFX");				//Nice, doesn't scale very well


			// Remove the parts of the original Vagrant bomb that we'd rather not have
			Destroy(nukePrefab.GetComponent<RoR2.Projectile.ProjectileSteerTowardTarget>());
			Destroy(nukePrefab.GetComponent<RoR2.Projectile.ProjectileDirectionalTargetFinder>());

			SphereCollider sc = nukePrefab.GetComponent<SphereCollider>();
			sc.radius = 2.0f; // Original prefab radius is 1.0f - EDIT made this 2 instead of 2.5, makes ita little easier to use near terrain

			MissileDroneSurvivor.MsIsleEntityStates.NukeAbility.projectilePrefabNuke = nukePrefab;


			// Networking
			if (!nukePrefab.GetComponent<NetworkIdentity>()) {
				nukePrefab.AddComponent<NetworkIdentity>();
			}
			ProjectileCatalog.getAdditionalEntries += list => list.Add(nukePrefab);
			nukePrefab.RegisterNetworkPrefab("Prefabs/MissileDroneMod/NukePrefabTestVagrant");

			Debug.Log("    >|\"Nuke\" projectile set up and assigned!");
		}

		/*		private void SetupNukeProjectile() {
					// Clone the default missile
					GameObject missilePrefab = Resources.Load<GameObject>("Prefabs/Projectiles/MissileProjectile");
					missilePrefab = missilePrefab.InstantiateClone("Prefabs/MissileDroneMod/NukePrefab");

					if (missilePrefab != null) {

						// Remove the tracking controller
						RoR2.Projectile.MissileController missileController = missilePrefab.GetComponent<RoR2.Projectile.MissileController>();
						Destroy(missileController);

						// Replace it with a simple projectile
						RoR2.Projectile.ProjectileSimple missileProjectileSimple = missilePrefab.AddComponent<RoR2.Projectile.ProjectileSimple>();
						missileProjectileSimple.lifetime = 25.0f;
						missileProjectileSimple.velocity = 10.0f;

						// Increase detonation force?
						RoR2.Projectile.ProjectileDamage projectileDamage = missilePrefab.GetComponent<RoR2.Projectile.ProjectileDamage>();
						projectileDamage.force = 100.0f;

						// Set proc coeff.
						RoR2.Projectile.ProjectileController projectileController = missilePrefab.GetComponent<RoR2.Projectile.ProjectileController>();
						projectileController.procCoefficient = 1.0f;

						// Remove the single-target damage component (moving to a radial attack)
						Destroy(missilePrefab.GetComponent<RoR2.Projectile.ProjectileSingleTargetImpact>());

						// Add an explosion radius
						RoR2.Projectile.ProjectileImpactExplosion projectileImpactExplosion = missilePrefab.AddComponent<RoR2.Projectile.ProjectileImpactExplosion>();


						// Possible effects?
						// - prefabs/effects/impacteffects
						// - prefabs/effects/MegaDroneDeathExplosion
						// - prefabs/effects/impacteffects/ExplosionSolarFlare
						// - prefabs/effects/VagrantNovaExplosion
						// - prefabs/effects/impacteffects/ExplosionVFX
						// - Prefabs/Effects/Omnieffect/OmniExplosionVFX

						projectileImpactExplosion.impactEffect = Resources.Load<GameObject>("Prefabs/Effects/Omnieffect/OmniImpactVFXLarge");
						projectileImpactExplosion.destroyOnEnemy = true;
						projectileImpactExplosion.destroyOnWorld = true;
						projectileImpactExplosion.timerAfterImpact = false;
						projectileImpactExplosion.falloffModel = BlastAttack.FalloffModel.Linear;
						projectileImpactExplosion.lifetime = 10.0f;
						projectileImpactExplosion.lifetimeAfterImpact = 0;
						projectileImpactExplosion.lifetimeRandomOffset = 0;
						projectileImpactExplosion.blastRadius = 35.0f;
						projectileImpactExplosion.blastDamageCoefficient = 5.0f;
						projectileImpactExplosion.blastProcCoefficient = 1;
						projectileImpactExplosion.blastAttackerFiltering = RoR2.AttackerFiltering.Default;
						projectileImpactExplosion.bonusBlastForce = new Vector3(0, 0, 3500);//(0, 0, 750, 0, 0, 0);
						projectileImpactExplosion.fireChildren = false;
						//projectileImpactExplosion.childrenProjectilePrefab =;
						projectileImpactExplosion.childrenCount = 0;
						projectileImpactExplosion.childrenDamageCoefficient = 0;
						projectileImpactExplosion.minAngleOffset = Vector3.zero;//(0, 0, 0, 0, 0, 0);
						projectileImpactExplosion.maxAngleOffset = Vector3.zero;//(0, 0, 0, 0, 0, 0);
						projectileImpactExplosion.transformSpace = RoR2.Projectile.ProjectileImpactExplosion.TransformSpace.World;
						//projectileImpactExplosion.projectileHealthComponent =;
						//projectileImpactExplosion.AuthorityEffect;

						if (missilePrefab != null) {
							MissileDroneSurvivor.MsIsleEntityStates.NukeAbility.projectilePrefabNuke = missilePrefab;
						}

						//MissileDroneSurvivor.MsIsleEntityStates.MissileBarrage.baseFireInterval = component.baseAttackSpeed;
					}
				}
				*/

		private void RegisterSkins() {
			Debug.Log("    >|Defining default skin...");

			//GameObject model = bodyComponent.GetComponentInChildren<ModelLocator>().modelBaseTransform.gameObject;
			GameObject model = msIsleCharacter.GetComponentInChildren<ModelLocator>().modelBaseTransform.gameObject;
			CharacterModel characterModel = model.GetComponentInChildren<CharacterModel>();
			if (!characterModel) { Debug.LogError("    >|CharacterModel not found!"); }

			// If the model already has a skin controller, delete it.
			ModelSkinController skinController = model.GetComponent<ModelSkinController>();
			if (skinController) {
				Destroy(skinController);
			}

			skinController = model.AddComponent<ModelSkinController>();
			SkinnedMeshRenderer mainRenderer;
			ChildLocator childLocator = model.GetComponent<ChildLocator>();
			if (!childLocator) {
				// ChildLocator doesn't seem to exist/be functional on the Missile Drone body, so just manually try to find the SkinnedMeshRenderer of the name "MissileDroneMesh"
				Debug.LogWarning("    >|ChildLocator not found! Manually attempting to find SkinnedMeshRenderer");
				//mainRenderer = model.GetComponentInChildren<SkinnedMeshRenderer>(false);
				mainRenderer = model.GetComponentInChildren<SkinnedMeshRenderer>(true);
				if (!mainRenderer) {
					Debug.LogError("    >|SkinnedMeshRenderer NOT FOUND IN CHILDREN!");
				}
			} else {
				mainRenderer = characterModel.GetFieldValue<SkinnedMeshRenderer>("mainSkinnedMeshRenderer");
				//mainRenderer = Reflection.GetFieldValue<SkinnedMeshRenderer>(characterModel, "mainSkinnedMeshRenderer");
			}

			LanguageAPI.Add("MSISLE_DEFAULT_SKIN_NAME", "Stock");
			LoadoutAPI.SkinDefInfo skinDefInfo = default(LoadoutAPI.SkinDefInfo);
			skinDefInfo.BaseSkins = Array.Empty<SkinDef>();
			skinDefInfo.GameObjectActivations = Array.Empty<SkinDef.GameObjectActivation>();
			skinDefInfo.Icon = LoadoutAPI.CreateSkinIcon(new Color(0.09f, 0.03f, 0.03f), new Color(0.039f, 0.039f, 0.078f), new Color(0.61f, 0.59f, 0.5f), new Color(0.9f, 0.9f, 0.9f));


			// This can also just be an empty array
			/*skinDefInfo.MeshReplacements = new SkinDef.MeshReplacement[] {
				new SkinDef.MeshReplacement {
					renderer = mainRenderer,
					mesh = mainRenderer.sharedMesh
				}
			};*/
			skinDefInfo.MeshReplacements = new SkinDef.MeshReplacement[0];

			skinDefInfo.Name = "MSISLE_DEFAULT_SKIN_NAME";
			skinDefInfo.NameToken = "MSISLE_DEFAULT_SKIN_NAME";
			skinDefInfo.RendererInfos = characterModel.baseRendererInfos;
			skinDefInfo.RootObject = model;
			skinDefInfo.UnlockableName = "";

			SkinDef defaultSkin = LoadoutAPI.CreateNewSkinDef(skinDefInfo);

			skinController.skins = new SkinDef[] {
				defaultSkin
			};
		}
	}
}